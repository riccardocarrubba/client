
import React from "react";
import Button from "react-bootstrap/Button";
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Modal from 'react-bootstrap/Modal';
import '../css/MostraListaStrutture.css';
import {FaEye} from 'react-icons/fa';
import {TiEdit} from 'react-icons/ti';
import {MdAddToPhotos} from 'react-icons/md';
import {RiDeleteBinLine} from "react-icons/ri";
import struttureService from '../services/strutture.service'
import authService from "../services/auth.service";
import {ToastContainer, toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {Config} from '../config/config'


export default class GestioneStruttura extends React.Component {
    state = {
        listStructures: [],
        show: false,
        index: undefined,
        struttura_id: undefined
    }
    constructor(props) {
        super(props)
        this.loadData()
    }
    loadData = async () => {
        try {
            let user = authService.getCurrentUser()
            let response = await struttureService.getAllStructures(user.id)
            this.setState({listStructures: response.data})
            this.handleShow(false)
        } catch (e) {
        }
    }
    eliminaStruttura = async () => {
        try {
            const {index,struttura_id} = this.state;
            let im = await struttureService.deleteImages({"id_struttura":struttura_id,"path": this.state.listStructures[index].paths });
            let response = await struttureService.deleteStructure(struttura_id);
            let strutture = this.state.listStructures;
            await toast("La struttura " + strutture[index].nome + " è stata eliminata");
            strutture.splice(index, 1);
            this.setState({listStructures: strutture, show: false})
        } catch (e) {
            toast(e.response.data);
        }
    }


    handleShow = (state) => {this.setState({show: state})}



    render() {
        return (
            <div id="listastrutture" className="container">

                <ToastContainer/>
                            {this.state.listStructures.map((struttura, index) =>
                                <Row className="lista-strutture-ricerca" key = {index}>
                                    <Col className="immagine" xs="12" sm="4" md="4" lg="4">
                                        <img variant="top" className="card-img" src={ struttura.paths.length > 0 ? Config.IMAGE_URL + struttura.paths[0].path : null} />
                                    </Col>
                                    <Col className="descrizione" xs="12" sm="5" md="5" lg="7">
                                        <Row>
                                            <span><strong fontSize={20}>{struttura.nome}</strong></span>
                                        </Row>
                                        <Row>
                                            <p> {struttura.descrizione} </p>
                                        </Row>
                                    </Col>
                                    <Col xs="6" sm="3" md="3" lg="1">
                                        <Row>
                                            <Button className="card-struttura-button" variant="primary small-button" type="button"
                                            onClick={() => this.props.history.push("/infoStruttura", struttura)}><FaEye  fontSize={23}/>
                                            </Button>
                                        </Row>
                                        <Row>
                                            <Button className="card-struttura-button" variant="primary small-button" type="button"
                                            onClick={() => this.props.history.push("/modificaStruttura", struttura)}><TiEdit fontSize={23} />
                                            </Button>
                                        </Row>
                                        <Row>
                                            <>
                                                <Button className="card-struttura-button" variant="danger small-button" type="button"
                                                        onClick={() => { this.handleShow(true)} }
                                                        onMouseDown={() => {this.setState({index: index,struttura_id: struttura.id})}}>
                                                    <RiDeleteBinLine fontSize={23}/>
                                                </Button>


                                            </>
                                        </Row>
                                    </Col>
                                </Row>

                            )}
                             <Modal show={this.state.show} onHide={() => this.handleShow(false)}>
                                                    <Modal.Header closeButton>
                                                        <Modal.Title>Attenzione</Modal.Title>
                                                    </Modal.Header>
                                                    <Modal.Body>Sei sicuro di voler eliminare questa struttura?</Modal.Body>
                                                    <Modal.Footer>
                                                        <Button variant="primary small-button" onClick={() => { this.handleShow(false)} } >
                                                            Chiudi
                                                        </Button>
                                                        <Button variant="danger small-button" onClick={() => {this.eliminaStruttura()}}>
                                                            Elimina
                                                        </Button>
                                                    </Modal.Footer>
                                                </Modal>
                             <Card style={{ width: '10rem' }} id="aggiungistruttura">
                            <Card.Body id="cardaggiungistruttura">
                                <Card.Title>
                                    <MdAddToPhotos fontSize={30} />
                                </Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">
                                    <Card.Link id="cardproprietariolink" href="/aggiungiStruttura" >Aggiungi Struttura </Card.Link>
                                </Card.Subtitle>
                            </Card.Body>
                        </Card>
            </div>
        );
    }
};
