import React from "react";
import "../css/footer.css";
import {GoMail} from "react-icons/go";
import {BsInfoCircle} from "react-icons/bs";
import {GiPositionMarker} from "react-icons/gi";
import {FaUser} from "react-icons/fa";
import {IoIosCall} from "react-icons/io";
import {MdAddCircleOutline} from "react-icons/md";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebook, faTwitter, faInstagram } from "@fortawesome/free-brands-svg-icons";
import {Row, Col} from "react-bootstrap";

export default class FooterPage extends React.Component{
    render() {
  return (
        <div className="main-footer">
            <div className="container" id="footer">
                <Row>
                    {/* Column 1 */}
                    <Col xs="12" sm="12" md="6" lg="4">
                        <hr />
                        <h4><GoMail /> CONTATTI </h4>
                        <hr />

                        <ul className="list-unstyled" style={{ padding: '0px' }}>
                            <li style={{ fontSize: 11 }}><FaUser /> laura.barbato01@community.unipa.it </li>
                            <li style={{ fontSize: 11 }}><FaUser /> fabio.cognata@community.unipa.it </li>
                            <li style={{ fontSize: 11 }}><FaUser /> ishraqhossain.akan@community.unipa.it </li>
                            <li style={{ fontSize: 11 }}><FaUser /> marcoaurelio.macaluso@community.unipa.it </li>
                            <li style={{ fontSize: 11 }}><FaUser /> riccardo.carrubba01@community.unipa.it </li>
                        </ul>
                    </Col>
                    {/* Column 2 */}
                    <Col xs="12" sm="6" md="3" lg="4">
                        <hr />
                        <h4><BsInfoCircle /> INFO</h4>
                        <hr />
                        <ul className="list-unstyled">
                            <li><GiPositionMarker /> Viale delle Scienze ed. 9,
                             90128 PALERMO (PA)</li>
                            <li><IoIosCall /> 0934 34562</li>
                        </ul>
                    </Col>
                    {/* Column 3 */}
                    <Col xs="12" sm="6" md="3" lg="4" >
                        <hr />
                        <h4><MdAddCircleOutline /> SEGUICI</h4>
                        <hr />
                        <ul className="list-unstyled">
                        <li style={{display: "inline"}}><a
                        href="https://www.facebook.com"
                        className="facebook social"
                        >
                        <FontAwesomeIcon icon={faFacebook} size="2x" />
                        </a></li>
                        <li style={{display: "inline"}}>
                        <a href="https://www.twitter.com" className="twitter social">
                        <FontAwesomeIcon icon={faTwitter} size="2x" />
                        </a>
                        </li>
                        <li style={{display: "inline"}}><a
                        href="https://www.instagram.com"
                        className="instagram social"
                        >
                        <FontAwesomeIcon icon={faInstagram} size="2x" />
                        </a></li>
                        </ul>
                    </Col>
                </Row>
                <hr />
            <div className="row" id="copy">
                <p className="col-sn">
                    &copy; {new Date().getFullYear()} | Università degli studi di Palermo | Progetto Indigo
                </p>
            </div>
            </div>
        </div>
  );
    }
}
