import React from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import * as yup from "yup";
import {Formik} from "formik";
import Card from "react-bootstrap/Card";
import InputGroup from "react-bootstrap/InputGroup";
import Col from "react-bootstrap/Col";
import CamereService from "../services/camere.service";
import {ToastContainer, toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const schema = yup.object().shape({
    nome: yup.string().required("Per favore, inserisci il nome della camera."),
    prezzo: yup.string().required("Inserisci il prezzo della camera").matches(/^[0-9]\d*(\.\d+)?$/, "Il prezzo inserito non può essere negativo"),
    descrizione: yup.string().required("Inserisci la descrizione della camera"),
});

export default class FormAggiungiCamera extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
          file: "",
          struttura: props.location.state,
          checkFiles: true,
        };
    }
    handlerChange = async (event) => {
       let files = event.target.files;
       let checkFiles=false;
        for await (let file of files){
            let filename =file.name.split(".").pop();
            let filenamee = filename.toLowerCase()
             if(filenamee !== 'png' && filenamee !== 'jpg' && filenamee !== 'jpeg'){
                toast("si possono caricare solo immagini");
                checkFiles=true;
                files =[];
                break;
             }
        }
        this.setState({ file: files, checkFiles: checkFiles });

  };

    registraCamera = async (values) => {
        try {
            let data = new FormData();
            for (let i = 0; i < this.state.file.length; i++) {
                data.append("images", this.state.file[i]);
            }
            data.append("anotherdata", JSON.stringify(values));
            let response = await CamereService.createRoom(data);
             toast('La camera è stata caricata con successo');
             setTimeout(() => {
               this.props.history.goBack();
             }, 650);
        } catch (e) {
            toast(e.response.data);
        }
    };

    render() {
        return (
          <div className="container">
            {this.state.struttura !== undefined ? (
              <Formik
                validationSchema={schema}
                onSubmit={(values, { setSubmitting }) => {
                  this.registraCamera(values);
                  setSubmitting(false);
                }}
                initialValues={{
                  nome: "",
                  prezzo: "",
                  posti_letto: 1,
                  descrizione: "",
                  area_fumatori: false,
                  bagno_disabili: false,
                  animali: false,
                  id_struttura: this.state.struttura.id,
                }}
              >
                {({
                  handleSubmit,
                  handleChange,
                  handleBlur,
                  values,
                  touched,
                  isValid,
                  errors,
                  isSubmitting,
                }) => (
                  <div className="Container" id="registrazione">
                    <div className="wrapper">
                      <ToastContainer />
                      <Card id="registrazionecard">
                        <Card.Header id="cardheaderregistrazione">
                          Aggiungi Camera
                        </Card.Header>
                        <Card.Body>
                          <Form
                            noValidate
                            onSubmit={handleSubmit}
                            id="formregistrazione"
                          >
                            <Form.Row>
                              <Form.Group controlId="formBasicNome">
                                <Form.Control
                                  name="nome"
                                  type="text"
                                  placeholder="Nome Camera"
                                  value={values.nome}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  isInvalid={
                                    touched.nome && errors.nome ? "error" : null
                                  }
                                  className="SignUpFormControls"
                                  size="lg"
                                />
                                <Form.Control.Feedback
                                  className="FeedBack"
                                  type="invalid"
                                >
                                  {errors.nome}
                                </Form.Control.Feedback>
                              </Form.Group>
                              <Form.Group
                                as={Col}
                                md="4"
                                controlId="validationFormikUsername2"
                              >
                                <InputGroup>
                                  <InputGroup.Prepend>
                                    <InputGroup.Text id="prezzo">
                                      €
                                    </InputGroup.Text>
                                  </InputGroup.Prepend>
                                  <Form.Control
                                    placeholder="Prezzo"
                                    aria-describedby="inputGroupPrepend"
                                    name="prezzo"
                                    type="number"
                                    step="0.01"
                                    pattern="^\d+(?:\.\d{1,2})?$"
                                    min="0"
                                    value={values.prezzo}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    isInvalid={
                                      touched.prezzo && errors.prezzo
                                        ? "error"
                                        : null
                                    }
                                  />
                                  <Form.Control.Feedback type="invalid" tooltip>
                                    {errors.prezzo}
                                  </Form.Control.Feedback>
                                </InputGroup>
                              </Form.Group>
                            </Form.Row>

                            <Form.Row>
                              <Form.Group id="file">
                                <Form.File
                                  className="position-relative"
                                  required
                                  name="file"
                                  onChange={this.handlerChange}
                                  accept=".jpg,.png,.jpeg"
                                  id="validationFormik107"
                                />
                              </Form.Group>
                            </Form.Row>
                            <Form.Row>
                              <InputGroup>
                                <InputGroup.Prepend>
                                  <InputGroup.Text>Descrizione</InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control
                                  as="textarea"
                                  name="descrizione"
                                  size="lg"
                                  className="SignUpFormControls"
                                  type="text"
                                  value={values.descrizione}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  isInvalid={
                                    touched.descrizione && errors.descrizione
                                      ? "error"
                                      : null
                                  }
                                  placeholder=""
                                />
                                <Form.Control.Feedback
                                  className="FeedBack"
                                  type="invalid"
                                >
                                  {errors.descrizione}
                                </Form.Control.Feedback>
                              </InputGroup>
                            </Form.Row>

                            <Form.Row id="varicheck">
                              <fieldset>
                                <Form.Group
                                  id="ospiti"
                                  controlId="formGridState"
                                >
                                  <Form.Label as="legend">
                                    Numero di ospiti
                                  </Form.Label>
                                  <Form.Control
                                    as="select"
                                    name="posti_letto"
                                    value={values.posti_letto}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                  >
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                  </Form.Control>
                                </Form.Group>
                              </fieldset>
                              <Form.Group
                                id="servizi"
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <Form.Label as="legend">Servizi:</Form.Label>
                                <Form.Check
                                  label="area fumatori"
                                  id="area_fumatori"
                                  name="area_fumatori"
                                  value={values.area_fumatori}
                                />
                                <Form.Check
                                  label="Bagno per disabili"
                                  id="bagno_disabili"
                                  name="bagno_disabili"
                                  value={values.bagno_disabili}
                                />
                                <Form.Check
                                  label="Animali ammessi"
                                  id="animali"
                                  name="animali"
                                  value={values.animali}
                                />
                              </Form.Group>
                            </Form.Row>
                            <Form.Row>
                              <Form.Group>
                                <Button
                                  variant="primary"
                                  type="submit"
                                  disabled={
                                    this.state.checkFiles ? true : false
                                  }
                                >
                                  Aggiungi
                                </Button>
                              </Form.Group>
                            </Form.Row>
                          </Form>
                        </Card.Body>
                      </Card>
                    </div>
                  </div>
                )}
              </Formik>
            ) : (
              <div> Nessuna struttura selezionata</div>
            )}
          </div>
        );
    }
}
