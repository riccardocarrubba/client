import authService from "../services/auth.service";
import BookService from "../services/book.service";
import Card from "react-bootstrap/Card";
import "../css/AreaPersonale.css";
import Form from "react-bootstrap/Form";
import { BsCardList, BsClock } from "react-icons/bs";
import React, { Component } from "react";
import { MdEuroSymbol } from "react-icons/md";
import { AiOutlineDollarCircle } from "react-icons/ai";

export default class GestionePrenotazione extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: "",
      guadagni: "",
    };
    this.loadData();
  }

  loadData = async () => {
    try{
      let user = authService.getCurrentUser();
      let response = await BookService.verificaGuadagni(user.id);
      this.setState({ guadagni: response.data });
    }
    catch(e){
    }
  };

  render() {
    return (
      <div className="container">
        <Form.Row>
          <Card style={{ width: "20rem" }} id="guadagni">
            <Card.Body id="cardGuadagni">
              <Card.Title>
                <AiOutlineDollarCircle fontSize={30} />
              </Card.Title>
              <Card.Text>
                Guadagni Attuali : {this.state.guadagni}
                <MdEuroSymbol id="prezzob" fontSize={30} />
              </Card.Text>
            </Card.Body>
          </Card>
        </Form.Row>
        <Form.Row id="cardproprietario" className="wrapper">
          <Card style={{ width: "20rem" }} id="datipersonali">
            <Card.Body id="carddatipersonali">
              <Card.Title>
                <BsClock fontSize={30} />
              </Card.Title>
              <Card.Subtitle className="mb-2 text-muted">
                <Card.Link
                  id="cardproprietariolink"
                  href="/storicoPrenotazioni"
                >
                  Storico Prenotazioni
                </Card.Link>
              </Card.Subtitle>
              <Card.Text>
                Visualizza lo storico delle prenotazioni accettate e terminate.
              </Card.Text>
            </Card.Body>
          </Card>
          <Card style={{ width: "20rem" }} id="gestionestruttura">
            <Card.Body id="cardgestionestruttura">
              <Card.Title>
                <BsCardList fontSize={30} />
              </Card.Title>
              <Card.Subtitle className="mb-2 text-muted">
                <Card.Link
                  id="cardproprietariolink"
                  href="/prenotazioniPending"
                >
                  Prenotazioni In Pending
                </Card.Link>
              </Card.Subtitle>
              <Card.Text>
                Visualizza la lista delle prenotazioni da accettare o rifiutare.
              </Card.Text>
            </Card.Body>
          </Card>
        </Form.Row>
        <Form.Row id="cardproprietario" className="wrapper"></Form.Row>
      </div>
    );
  }
}
