import React from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import * as yup from "yup";
import { Formik } from "formik";
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import '../css/GestionePrenotazioni.css';
import {MdEuroSymbol} from 'react-icons/md';
import authService from "../services/auth.service";
import "react-toastify/dist/ReactToastify.css";
import BookService from "../services/book.service";
import { ToastContainer, toast } from "react-toastify";

const validate_birthday = yup.string().required("La data deve essere immessa")
  .test({
    name: "DOB",
    message: "Devi aver compiuto almeno 18 anni!",
    test: (value) => {
      let now = Date.now();
      let date = Date.parse(value);
      return new Date(now - date).getUTCFullYear() - 1970 > 18;
    },
  });

const schema = yup.object({
  nome: yup.string().required('Per favore, inserisci un nome.'),
  cognome: yup.string().required('Per favore, inserisci un cognome.'),
  datadinascita: validate_birthday,
  numeroospiti: yup.string().required("Devi specificare il numero di ospiti"),
  email: yup.string().email().required('Per favore, inserisci la tua email'),
  descrizione: yup.string(),
  tassa: yup.bool(),
});

export default class RichiestaPrenotazione extends React.Component{

  constructor(props) {
    super(props)

    if(props.location.state !== undefined){
      this.state = {
        check_in: props.location.state.check_in,
        check_out: props.location.state.check_out,
        room: props.location.state.struttura,
        permanenza_giorni: props.location.state.permanenza_giorni,
        prezzo: props.location.state.prezzo,
        prezzo_finale: "",
        isSubmitting:false,
    }
    }
}

calcolaPrezzo = (values) => {
  if(values.tassa === true){
    this.setState({ prezzo_finale: this.state.prezzo * this.state.permanenza_giorni})
  }else if((values.tassa === false )&& (values.tipologia_ospite === "2")){
    this.setState({ prezzo_finale: this.state.prezzo* this.state.permanenza_giorni})
  }else if((values.tassa === true) && (values.tipologia_ospite === "2")){
    this.setState({ prezzo_finale: this.state.prezzo* this.state.permanenza_giorni})
  }else if(values.tassa === false){
    this.setState({ prezzo_finale: ((this.state.prezzo + this.state.room.tassa)*(this.state.permanenza_giorni)).toFixed(2)})
  }
}
handleSubmit = async (values) => {
  let data = {...values}
  if (this.state.room.tipologia === 1) {
    data.id_utente = authService.getCurrentUser().id;
    data.id_camera = this.state.room.id;
    data.id_struttura = this.state.room.id_struttura;
    data.prezzo = (this.state.prezzo * this.state.permanenza_giorni);
    data.richieste = values.descrizione;
    try {
      let response = await BookService.createBook(data);
      toast("prenotazione avvenuta con successo");
      setTimeout(() => {
        this.props.history.push("/home");
      }, 4000);
    } catch (e) {
      toast(e.response.data);
    }
  }
  else if (this.state.room.tipologia === 2) {
    data.id_utente = authService.getCurrentUser().id;
    data.id_camera = null;
    data.id_struttura = this.state.room.id;
    data.prezzo = this.state.prezzo;
    data.richieste = values.descrizione;
    try {
      let response = await BookService.createBook(data);
      toast("Prenotazione avvenuta con successo");
      setTimeout(() => {
        this.props.history.push("/home");
      }, 4000);
    } catch (e) {
      toast(e.response.data);
    }
  }
};
verifyDate = async (values) => {
  try {
    const {check_in, check_out}= values;
    const id_utente = authService.getCurrentUser().id;
    const id = this.state.room.id;
    const tipologia = this.state.room.tipologia
    let date;
    date = await BookService.verifyDate({check_in, check_out,id_utente,tipologia, id});
    if (parseInt(date.data.giorni,10) + this.state.permanenza_giorni <= 28) {
      return true;
    } else {
      return false;
    }
  } catch (e) {
    return null
  }
}

render(){
  return (

    <div className="container">
      <ToastContainer />
      {this.state ?
        <Formik
    validationSchema={schema}
    onSubmit={async (values, { setSubmitting }) => {
      try{
        this.calcolaPrezzo(values);
        let bool = await this.verifyDate(values);
        if(bool === true){
            this.handleSubmit(values);
            this.setState({isSubmitting: true})
        }else{
          toast("prenotazione non avvenuta, massimo annuale nella struttura di 28 giorni superato");
        }
      }
      catch(e){
      }
    }}
    initialValues={{
      nome: authService.getCurrentUser().nome,
      cognome:authService.getCurrentUser().cognome,
      email :authService.getCurrentUser().mail,
      datadinascita:authService.getCurrentUser().data_nascita,
      tipologia_ospite: 1,
      numeroospiti: this.state.room.posti_letto,
      descrizione: "",
      check_in: this.state.check_in,
      check_out: this.state.check_out,
      tassa: false,
    }}>
  {({
    handleSubmit,
    handleChange,
    handleBlur,
    values,
    touched,
    isValid,
    errors,
  }) => (
    <div className="Container" id = "registrazione">
      <Card id="registrazionecard">
        <Card.Header id="cardheaderregistrazione">Richiesta Prenotazione</Card.Header>
          <Card.Body>
        <Form noValidate onSubmit={handleSubmit} id="formregistrazione">
          <Form.Row>
              <Col xs="12" sm="6" md="6" lg="3">
                <Form.Label as="legend" >Nome: </Form.Label>
                <Form.Group>
                  <Form.Control
                  type="text"
                  placeholder="Nome"
                  value={values.nome}
                  onChange={handleChange}
                  name="nome"
                  className="SignUpFormControls"
                  size="lg"
                  isInvalid={!!errors.nome}
                  />
                <Form.Control.Feedback className="FeedBack" type="invalid">
              {errors.nome}
            </Form.Control.Feedback>
          </Form.Group>
          </Col>
          <Col xs="12" sm="6" md="6" lg="3">
          <Form.Label as="legend" >Cognome: </Form.Label>
        <Form.Group>
          <Form.Control
            type="text"
            placeholder="Cognome"
            value={values.cognome}
            onChange={handleChange}
            name="cognome"
            className="SignUpFormControls"
            size="lg"
            isInvalid={!!errors.cognome}
          />
          <Form.Control.Feedback className="FeedBack" type="invalid">
          {errors.cognome}
        </Form.Control.Feedback>
        </Form.Group>
        </Col>
        <Col xs="12" sm="6" md="6" lg="3">
                <Form.Label as="legend" >Data di nascita: </Form.Label>
        <Form.Group id="datadinascitaprenotazione">

            <Form.Control
            size="lg"
            className="SignUpFormControls"
            type="date"
            name="datadinascita"
            value={values.datadinascita}
            onChange={handleChange}
            placeholder="Data di nascita"
            isInvalid={!!errors.datadinascita}
          />
          <Form.Control.Feedback className="FeedBack" type="invalid">
          {errors.datadinascita}
        </Form.Control.Feedback>
        </Form.Group>
        </Col>
        <Col xs="12" sm="6" md="6" lg="3">
            <Form.Label as="legend">Posti letto: </Form.Label>
        <Form.Group controlId="formGridState">
          <Form.Control
          disabled
            type="text"
            value={values.numeroospiti}
            placeholder="Ospiti: ">
          </Form.Control>
        </Form.Group>
        </Col>
        </Form.Row>
        <Form.Row>
          <Col xs="12" sm="6" md="6" lg="3">
            <Form.Label>Check-in</Form.Label>
              <Form.Control
                disabled
                size="lg"
                type="text"
                name="check_in"
                value={values.check_in}
                 />
            <Form.Label>Check-out:</Form.Label>
              <Form.Control
                disabled
                size="lg"
                type="text"
                name="check_out"
                value={values.check_out}
                />
          </Col>
          <Col  xs="12" sm="12" md="12" lg="12">
          <Form.Group>
            <Form.Label as="legend">Tipologia di Ospite:</Form.Label>
              <Form.Control
              as="select"
              name="tipologia_ospite"
              value={values.tipologia_ospite}
              onChange={handleChange}
              >
                <option value="1">Esente da tassa di soggiorno</option>
                <option value="2">Turista</option>
              </Form.Control>
                <p>La tassa di soggiorno non può però essere addebitata ad alcune categorie di viaggiatori. Questi sono: i residenti nel comune,
                    i bambini di età inferiore a 12 anni, gli studenti universitari fuori sede, i disabili e gli accompagnatori, i militari e le forze di polizia,
                    autisti di autobus, treni e accompagnatori turistici, i malati in strutture sanitarie e i familiari accompagnatori, i residenti negli ostelli della gioventù</p>
            </Form.Group>
            </Col>
            </Form.Row>
            <Form.Row>
              <Col  xs="12" sm="12" md="6" lg="6">
            <Form.Label as="legend">Richieste Particolari:</Form.Label>
              <Form.Control
                as="textarea"
                name="descrizione"
                size="lg"
                className="SignUpFormControls"
                type="text"
                value={values.descrizione}
                onChange={handleChange}
                onBlur={handleBlur}
                isInvalid={touched.descrizione && errors.descrizione ? "error" : null}
                placeholder=""/>
              <Form.Control.Feedback
                className="FeedBack"
                type="invalid">{errors.descrizione}
              </Form.Control.Feedback>
              <Form.Group>
                {
                  values.tipologia_ospite === "2" ?
                    <Form.Check
                    name="tassa"
                    label="Pago la tassa di soggiorno in struttura"
                    onChange={handleChange}
                    value={values.tassa}
                    isInvalid={!!errors.tassa}
                    feedback={errors.tassa}
                  />
                  : null
                }
              </Form.Group>

              </Col>
              <Col  xs="12" sm="12" md="6" lg="6">
                <h3>Totale da pagare: </h3>
                <span>Pagando la tassa online:</span>
                <h3>{((this.state.prezzo + this.state.room.tassa) * (this.state.permanenza_giorni)).toFixed(2)}<MdEuroSymbol id="prezzo" fontSize={20} /></h3>
                <span>Pagando la tassa in struttura/Esente da tassa: </span>
                <h3>{((this.state.prezzo) * (this.state.permanenza_giorni)).toFixed(2)}<MdEuroSymbol id="prezzo" fontSize={20} /></h3>
              </Col>
            </Form.Row>
            <Form.Row>
              <Form.Group>
                <Button id="richiesta-prenotazione-button" variant="primary" type="submit" disabled={this.state.isSubmitting}>
                  Avanti
                </Button>
              </Form.Group>
            </Form.Row>
          </Form>
        </Card.Body>
      </Card>
          </div>)}
    </Formik>
    :
    <div> nessuna camera o struttura selezionata </div> }


  </div>
  );
}
};
