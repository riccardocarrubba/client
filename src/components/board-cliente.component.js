
import React, { Component } from "react";
import Card from 'react-bootstrap/Card';
import '../css/AreaPersonale.css';
import Form from 'react-bootstrap/Form';
import {FaRegAddressCard} from 'react-icons/fa';
import {MdWork} from 'react-icons/md';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import AuthService from "../services/auth.service";


export default class BoardCliente extends Component {
                 constructor(props) {
                   super(props);

                   this.state = {
                     content: "",
                     currentUser: AuthService.getCurrentUser()
                   };
                 }

                 render() {
                  const { currentUser } = this.state;
                    return(
                      <div className="container">
                          <div id="profilo">
                              <h1 id="ciao">Ciao, {currentUser.nome}!</h1>
                              <Row>
                                  <Col xs="12" sm="12" md="12" lg="6">
                                      <p id="profilo"><strong>Nome: </strong>{currentUser.nome}</p>
                                  </Col>
                                  <Col xs="12" sm="12" md="12" lg="6">
                                      <p id="profilo"><strong>Cognome: </strong>{currentUser.cognome}</p>
                                  </Col>
                              </Row>
                              <Row>
                                  <Col xs="12" sm="12" md="12" lg="6">
                                      <p id="profilo"><strong>Data di nascita: </strong>{currentUser.data_nascita}</p>
                                  </Col>
                                  <Col xs="12" sm="12" md="12" lg="6">
                                      <p id="profilo"><strong>Città: </strong>{currentUser.citta}</p>
                                  </Col>
                              </Row>
                              <Row>
                                  <Col xs="12" sm="12" md="12" lg="6">
                                      <p id="profilo"><strong>Numero di telefono :</strong>{currentUser.telefono}</p>
                                  </Col>
                                  <Col xs="12" sm="12" md="12" lg="6">
                                      <p id="profilo"><strong>Email: </strong>{currentUser.mail}</p>
                                  </Col>
                              </Row>
                          </div>
                      <Form.Row id="cardcliente">
                      <Card style={{ width: '22rem' }} id="datipersonali">
                          <Card.Body id="carddatipersonali">
                              <Card.Title>
                              <FaRegAddressCard fontSize={30}/>
                              </Card.Title>
                              <Card.Subtitle className="mb-2 text-muted"><Card.Link id="cardutentelink" href="datiPersonali">Modifica dati Personali </Card.Link></Card.Subtitle>
                              <Card.Text>
                              Visualizza o modifica i tuoi dati personali.
                              </Card.Text>
                          </Card.Body>
                      </Card>
                      <Card style={{ width: '22rem' }} id="prenotazionieffettuate">
                          <Card.Body id="cardprenotazionieffettuate">
                              <Card.Title>
                              <MdWork fontSize={30} />
                              </Card.Title>
                              <Card.Subtitle className="mb-2 text-muted">
                              <Card.Link id="cardutentelink" href="/prenotazioniEffettuate" >Prenotazioni Effettuate </Card.Link>
                              </Card.Subtitle>
                              <Card.Text>
                              Visualizza le tue prenotazioni passate o che hai effettuato.
                              </Card.Text>
                          </Card.Body>
                      </Card>
                      </Form.Row>
                      </div>
                  )
                 }
               }
