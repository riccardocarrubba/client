
import Card from 'react-bootstrap/Card';
import '../css/DatiLegali.css';
import Form from 'react-bootstrap/Form';
import {AiFillEuroCircle} from 'react-icons/ai';
import {GiPoliceOfficerHead} from 'react-icons/gi';
import React, { Component } from "react";

export default class DatiLegali extends Component {
                 constructor(props) {
                   super(props);

                   this.state = {
                     content: "",
                   };
                 }
                 render() {
                   return (
                    <div className="container">
                    <Form.Row id="cardproprietarioriga2">
                        <Card style={{ width: '22rem' }} id="comunicazioniallaquestura">
                            <Card.Body id="cardcomunicazioniquestura">
                                <Card.Title>
                                    <GiPoliceOfficerHead fontSize={30} />
                                </Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">
                                    <Card.Link id="cardproprietariolink" href="/datiQuesturaCliente" >Comunicazioni alla questura</Card.Link>
                                </Card.Subtitle>
                                <Card.Text>
                                Invia i dati dei tuoi ospiti alla Questura.
                                </Card.Text>
                            </Card.Body>
                        </Card>
                            <Card style={{ width: '22rem' }} id="rendicontotrimestrale">
                                <Card.Body id="cardrendicontotrimestrale">
                                    <Card.Title>
                                        <AiFillEuroCircle fontSize={30} />
                                    </Card.Title>
                                    <Card.Subtitle className="mb-2 text-muted">
                                        <Card.Link id="cardproprietariolink" href="/rendicontoTrimestrale" >Rendiconto Trimestrale</Card.Link>
                                    </Card.Subtitle>
                                    <Card.Text>
                                    Comunica all'ufficio del turismo l'ammontare delle tasse di soggiorno,
                                    la generalità degli ospiti e il periodo.
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                    </Form.Row>
                </div>
                   );
                 }
               }
