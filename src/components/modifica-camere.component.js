import React from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import * as yup from "yup";
import { Formik } from "formik";
import Card from "react-bootstrap/Card";
import InputGroup from "react-bootstrap/InputGroup";
import Col from "react-bootstrap/Col";
import CamereService from "../services/camere.service";
import { ToastContainer, toast } from "react-toastify";
import { Config } from "../config/config";
import { Row } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import { TiEdit } from "react-icons/ti";

import "react-toastify/dist/ReactToastify.css";

const schema = yup.object().shape({
    nome: yup.string().required("Per favore, inserisci il nome della camera."),
    prezzo: yup.string().required("Inserisci il prezzo della camera").matches(/^[0-9]\d*(\.\d+)?$/, "Il prezzo inserito non può essere negativo"),
    descrizione: yup.string().required("Inserisci la descrizione della camera"),
});

export default class ModificaCamera extends React.Component {
  constructor(props) {
    super(props);
    try {
      this.state = {
        file: "",
        camera: props.location.state,
        checkFiles: false,
      };
    } catch (e) {
      this.state = {
        file: "",
        camera: null,
      };
    }
  }

      handlerChange = async (event) => {
       let files = event.target.files;
       let checkFiles=false;
        for await (let file of files){
            let filename =file.name.split(".").pop();
            let filenamee = filename.toLowerCase()
             if(filenamee !== 'png' && filenamee !== 'jpg' && filenamee !== 'jpeg'){
                toast("si possono caricare solo immagini");
                checkFiles=true;
                files =[];
                break;
             }
        }
        this.setState({ file: files, checkFiles: checkFiles });
  };

  handleShow = (state) => {
    this.setState({ show: state });
  };

  aggiornaFoto = async (values) => {
    try {
      let data = new FormData();
      for (let i = 0; i < this.state.file.length; i++) {
        data.append("images", this.state.file[i]);
      }
      data.append("anotherdata", this.state.camera.id);
      data.append("room", JSON.stringify(values));
      let res = await CamereService.updateImages(data);
      this.setState({camera: res.data})
      this.setState({show: false})
      await toast("Le immagini sono state aggiornate con successo");
    } catch (e) {
      toast(e.response.data);
    }
  };
  handleChangeServizi = (e) => {
    const animali = document.getElementById("animali");
    const area_fumatori = document.getElementById("area_fumatori");
    const bagno_disabili = document.getElementById("bagno_disabili");
    let str = this.state.camera;
    str.animali = animali.checked;
    str.area_fumatori = area_fumatori.checked;
    str.bagno_disabili = bagno_disabili.checked;
    this.setState({ strutture: str });
  };

  render() {
    return (
      <div className="container">
        {this.state.camera !== undefined ? (
          <Formik
            validationSchema={schema}
            onSubmit={async (values) => {
              try {
                values.path = this.state.camera.path;
                let response = await CamereService.modifyRooms(values);
                setTimeout(() => {
                  this.props.history.goBack();
                }, 650);
                toast("la camera è stata correttamente aggiornata");
              } catch (e) {
                toast(e.response.data);
              }
            }}
            initialValues={this.state.camera}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              values,
              touched,
              isValid,
              errors,
              isSubmitting,
            }) => (
              <div className="Container" id="registrazione">
                <div className="wrapper">
                  <ToastContainer />
                  <Card id="registrazionecard">
                    <Card.Header id="cardheaderregistrazione">
                      Modifica Camera
                    </Card.Header>
                    <Card.Body>
                      <Form
                        noValidate
                        onSubmit={handleSubmit}
                        id="formregistrazione"
                      >
                        <Form.Row>
                          <Form.Group controlId="formBasicNome">
                              <Form.Label>Nome:</Form.Label>
                            <Form.Control
                              name="nome"
                              type="text"
                              placeholder="Nome Camera"
                              value={values.nome}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              isInvalid={
                                touched.nome && errors.nome ? "error" : null
                              }
                              className="SignUpFormControls"
                              size="lg"
                            />
                            <Form.Control.Feedback
                              className="FeedBack"
                              type="invalid"
                            >
                              {errors.nome}
                            </Form.Control.Feedback>
                          </Form.Group>
                          <Form.Group
                            as={Col}
                            md="4"
                            controlId="validationFormikUsername2"
                          >
                              <Form.Label>Prezzo:</Form.Label>
                            <InputGroup>
                              <InputGroup.Prepend>
                                <InputGroup.Text id="prezzo">€</InputGroup.Text>
                              </InputGroup.Prepend>
                              <Form.Control
                                placeholder="Prezzo"
                                aria-describedby="inputGroupPrepend"
                                name="prezzo"
                                type="number"
                                step="0.01"
                                pattern="^\d+(?:\.\d{1,2})?$"
                                min="0"
                                value={values.prezzo}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isInvalid={
                                  touched.prezzo && errors.prezzo
                                    ? "error"
                                    : null
                                }
                              />
                              <Form.Control.Feedback type="invalid" tooltip>
                                {errors.prezzo}
                              </Form.Control.Feedback>
                            </InputGroup>
                          </Form.Group>
                        </Form.Row>

                        <Form.Row>
                          <div>
                            <img
                              src={Config.IMAGE_URL + this.state.camera.path}
                              alt=""
                              className="img-responsive"
                              id="elimina-immagine-struttura"
                            />
                          </div>
                          <Button
                            id="modificaFoto"
                            className="card-struttura-button"
                            variant="primary small-button"
                            type="button"
                            onClick={() => {
                              this.handleShow(true);
                            }}
                          >
                            <TiEdit fontSize={23} />
                          </Button>

                          <Modal
                            show={this.state.show}
                            onHide={() => this.handleShow(false)}
                          >
                            <Modal.Header closeButton>
                              <Modal.Title>Modifica Foto</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              Se procedi con questa opzione , dovrai reinserire
                              la foto
                              <Row></Row>
                              <Row>
                                <div>
                                  <img
                                    src={
                                      Config.IMAGE_URL + this.state.camera.path
                                    }
                                    alt=""
                                    className="img-responsive"
                                  />
                                </div>
                                <Row>
                                  <input
                                    type="file"
                                    className="upload"
                                    accept=".jpg,.png,.jpeg"
                                    name="updateImage"
                                    onChange={this.handlerChange}
                                    onBlur={handleBlur}
                                  />
                                </Row>
                              </Row>
                            </Modal.Body>
                            <Modal.Footer>
                              <Button
                                variant="primary"
                                onClick={() => {
                                  this.handleShow(false);
                                }}
                              >
                                Chiudi
                              </Button>

                              {this.state.file.length > 0 ? (
                                <Button
                                  variant="danger"
                                  onClick={() => {
                                    this.aggiornaFoto(values);
                                  }}
                                >
                                  Aggiorna
                                </Button>
                              ) : (
                                <Button
                                  variant="danger"
                                  disabled
                                  onClick={() => {
                                    this.aggiornaFoto();
                                  }}
                                >
                                  Aggiorna
                                </Button>
                              )}
                            </Modal.Footer>
                          </Modal>
                        </Form.Row>
                        <Form.Row>
                          <InputGroup>
                            <InputGroup.Prepend>
                              <InputGroup.Text>Descrizione</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                              as="textarea"
                              name="descrizione"
                              size="lg"
                              className="SignUpFormControls"
                              type="text"
                              value={values.descrizione}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              isInvalid={
                                touched.descrizione && errors.descrizione
                                  ? "error"
                                  : null
                              }
                              placeholder=""
                            />
                            <Form.Control.Feedback
                              className="FeedBack"
                              type="invalid"
                            >
                              {errors.descrizione}
                            </Form.Control.Feedback>
                          </InputGroup>
                        </Form.Row>

                        <Form.Row id="varicheck">
                          <fieldset>
                            <Form.Group id="ospiti" controlId="formGridState">
                              <Form.Label as="legend">
                                Numero di ospiti
                              </Form.Label>
                              <Form.Control
                                as="select"
                                name="posti_letto"
                                value={values.posti_letto}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                              </Form.Control>
                            </Form.Group>
                          </fieldset>
                          <Form.Group
                            id="servizi"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            onClick={this.handleChangeServizi}
                          >
                            <Form.Label as="legend">Servizi:</Form.Label>
                            <Form.Check
                              label="area fumatori"
                              id="area_fumatori"
                              name="area_fumatori"
                              value={values.area_fumatori}
                              checked={values.area_fumatori}
                              onChange={() => {}}
                            />
                            <Form.Check
                              label="Bagno per disabili"
                              id="bagno_disabili"
                              name="bagno_disabili"
                              value={values.bagno_disabili}
                              checked={values.bagno_disabili}
                              onChange={() => {}}
                            />
                            <Form.Check
                              label="Animali ammessi"
                              id="animali"
                              name="animali"
                              value={values.animali}
                              checked={values.animali}
                              onChange={() => {}}
                            />
                          </Form.Group>
                        </Form.Row>
                        <Form.Row>
                          <Form.Group>
                            <Button
                              variant="primary"
                              type="submit"
                              disabled={this.state.checkFiles ? true : false}
                            >
                              Salva
                            </Button>
                          </Form.Group>
                        </Form.Row>
                      </Form>
                    </Card.Body>
                  </Card>
                </div>
              </div>
            )}
          </Formik>
        ) : (
          <div> Nessuna Camera selezionata</div>
        )}
      </div>
    );
  }
}
