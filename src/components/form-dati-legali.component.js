import React from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import {Formik} from "formik";
import * as yup from "yup";
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import '../css/FormDatiQuestura.css';
import {toast,ToastContainer} from "react-toastify";
import authService from "../services/auth.service";
import struttureService from "../services/strutture.service";
import customerService from "../services/customer.service";
import moment from "moment";


const validate_birthday = yup
  .string()
  .required("La data di nascita dell'utente deve essere immessa")
  .test({
    name: "DOB",
    message: "Deve aver compiuto almeno 18 anni!",
    test: (value) => {
      let now = Date.now();
      let date = Date.parse(value);
      return new Date(now - date).getUTCFullYear() - 1970 >= 18;
    },
  });
const schema = yup.object().shape({
    structure: yup.string().required("Per favore, inserisci il nome della struttura."),
    user: yup.object().shape({
        nome: yup.string().required("Per favore, inserisci il nome del utente."),
        cognome: yup.string().required("Per favore, inserisci il cognome del utente."),
            citta_nascita: yup.string().required("Per favore, inserisci il citta di nascita del utente."),
            citta_residenza: yup.string().required("Per favore, inserisci il citta di residenza del utente."),
            data_nascita: validate_birthday,
            check_in: yup.date().required("Il campo è obbligatorio").max(new Date(),"non può essere inserita una data maggiore di quella odierna"),
            check_out: yup.string().required("Il campo è obbligatorio").max(new Date(),"non può essere inserita una data maggiore di quella odierna")
    })
});


var DataMassimaNascita;
var DataMassimaOdierna;
export default class FormDatiQuesturaCliente extends React.Component {
  state = {
    structure: 0,
    user: {
      nome: "",
      cognome: "",
      citta_nascita: "",
      citta_residenza: "",
      data_nascita: "",
      check_in: "",
      check_out: "",
      file: "",
    },
    listStructures: [],
    checkFiles: true,
  };

  constructor(props) {
    super(props);
    this.loadData();
    this.getDate();
  }

  loadData = async () => {
    try {
      let user = authService.getCurrentUser();
      let response = await struttureService.getAllStructures(user.id);
      this.setState({ listStructures: response.data });
    } catch (e) {
    }
  };

  getDate = () => {
    var d = new Date(),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear() - 18,
      ThisYear = d.getFullYear();
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;
    this.DataMassimaNascita = `${[year, month, day].join("-")}`;
    this.DataMassimaOdierna = `${[ThisYear, month, day].join("-")}`;
  };

  render() {
    return (
      <div className="container" className="allScreen">
        <Formik
          initialValues={this.state}
          validationSchema={schema}
          onSubmit={async (values, { setSubmitting }) => {
            try {
              let d= new Date(Date.parse(values.user.check_out+'T00:00:00.000Z')).getTime()
              let d2= new Date(Date.parse(values.user.check_in+'T00:00:00.000Z')).getTime()
              if(d>d2){
                let formData = new FormData();
                let copy = { ...values.user };
                delete copy.file;
                formData.append(
                  "structure",
                  JSON.stringify(this.state.listStructures[values.structure])
                );
                formData.append("file", values.user.file);
                formData.append("user", JSON.stringify(copy));
                let response = await customerService.sendDataToPolice(formData);
                toast("I tuoi dati sono stati inviati correttamente");
                setTimeout(() => {
                    window.location.reload();
                },1000)
              }else{toast("il check-in non può essere maggiore del check-out")}
              
            } catch (e) {
              console.log(e)
            }
            setSubmitting(false);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            isSubmitting,
          }) => (
            <div>
              <ToastContainer />
              <Card>
                <Card.Header>Comunicazioni alla Questura</Card.Header>
                <Card.Body>
                   {this.state.listStructures.length > 0  ?
                  <Form onSubmit={handleSubmit}>
                    <div>
                      <Form.Group controlId="structure">
                        <Form.Label>Seleziona la struttura</Form.Label>
                        <Form.Control
                          as="select"
                          name="structure"
                          value={values.structure}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        >
                          {this.state.listStructures.map((struct, index) => (
                            <option key={index} value={index}>
                              {this.state.listStructures[index].nome}
                            </option>
                          ))}
                        </Form.Control>
                      </Form.Group>

                      <Form.Row>
                        <Form.Group controlId="nome">
                          <Form.Label>Nome</Form.Label>
                          <Form.Control
                            name="user.nome"
                            value={values.user.nome}
                            type="text"
                            placeholder="Nome"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            size="lg"
                            isInvalid={
                              touched.user && errors.user
                                ? touched.user.nome && errors.user.nome
                                  ? "error"
                                  : null
                                : null
                            }
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.user ? errors.user.nome : null}
                          </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group controlId="cognome">
                          <Form.Label>Cognome</Form.Label>
                          <Form.Control
                            name="user.cognome"
                            value={values.user.cognome}
                            type="text"
                            placeholder="Cognome"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            size="lg"
                            isInvalid={
                              touched.user && errors.user
                                ? touched.user.cognome && errors.user.cognome
                                  ? "error"
                                  : null
                                : null
                            }
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.user ? errors.user.cognome : null}
                          </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group controlId="data_nascita">
                          <Form.Label>Data di nascita</Form.Label>
                          <Form.Control
                            size="lg"
                            type="date"
                            max={this.DataMassimaNascita}
                            name="user.data_nascita"
                            value={values.user.data_nascita}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Data di nascita"
                            isInvalid={
                              touched.user && errors.user
                                ? touched.user.data_nascita &&
                                  errors.user.data_nascita
                                  ? "error"
                                  : null
                                : null
                            }
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.user ? errors.user.data_nascita : null}
                          </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group controlId="citta_nascita">
                          <Form.Label>Città di nascita</Form.Label>
                          <Form.Control
                            name="user.citta_nascita"
                            value={values.user.citta_nascita}
                            type="text"
                            placeholder="Citta di nascita"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            size="lg"
                            isInvalid={
                              touched.user && errors.user
                                ? touched.user.citta_nascita &&
                                  errors.user.citta_nascita
                                  ? "error"
                                  : null
                                : null
                            }
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.user ? errors.user.citta_nascita : null}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Form.Row>
                      <Form.Row>
                        <Form.Group controlId="citta_residenza">
                          <Form.Label>Città di residenza</Form.Label>
                          <Form.Control
                            name="user.citta_residenza"
                            value={values.user.citta_residenza}
                            type="text"
                            placeholder="Citta di residenza"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            size="lg"
                            isInvalid={
                              touched.user && errors.user
                                ? touched.user.citta_residenza &&
                                  errors.user.citta_residenza
                                  ? "error"
                                  : null
                                : null
                            }
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.user ? errors.user.citta_residenza : null}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="check_in">
                          <Form.Label>Check In</Form.Label>
                          <Form.Control
                            size="lg"
                            type="date"
                            max={this.DataMassimaOdierna}
                            name="user.check_in"
                            value={values.user.check_in}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Check in"
                            isInvalid={
                              touched.user && errors.user
                                ? touched.user.check_in && errors.user.check_in
                                  ? "error"
                                  : null
                                : null
                            }
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.user ? errors.user.check_in : null}
                          </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group controlId="file" style={{ margin: "10px" }}>
                          <Row>
                            <Form.Label>File</Form.Label>
                          </Row>

                          <Row>
                              <input
                                  style={{width: "100%"}}
                                  type="file"
                                  className="upload"
                                  accept=".pdf,.doc,.docx"
                                  name="user.file"
                                  onChange={(event) => {
                                      let checkFiles = false;
                                      let filename = event.target.files[0].name
                                          .split(".")
                                          .pop();
                                      let filenamee = filename.toLowerCase();
                                      if (
                                          filenamee === "pdf" ||
                                          filenamee === "doc" ||
                                          filenamee === "docx"
                                      ) {
                                          checkFiles = false;
                                          setFieldValue(
                                              `user.file`,
                                              event.target.files[0]
                                          );
                                      } else {
                                          checkFiles = true;
                                          toast(
                                              "si possono caricare solo documenti pdf , doc"
                                          );
                                      }
                                      this.setState({checkFiles: checkFiles});
                                  }}
                                  onBlur={handleBlur}
                                  required
                              />
                          </Row>

                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.user
                              ? errors.user.file
                                ? errors.user.file.name
                                : null
                              : null}
                          </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group controlId="check_out">
                          <Form.Label>Check Out</Form.Label>
                          <Form.Control
                            size="lg"
                            type="date"
                            max={this.DataMassimaOdierna}
                            name="user.check_out"
                            value={values.user.check_out}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Check out"
                            isInvalid={
                              touched.user && errors.user
                                ? touched.user.check_out &&
                                  errors.user.check_out
                                  ? "error"
                                  : null
                                : null
                            }
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.user ? errors.user.check_out : null}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Form.Row>
                    </div>

                    <div className="row-margin">
                      <Button
                        variant="primary"
                        type="submit"
                        className="float-right"
                        disabled={this.state.checkFiles ? true : false}
                      >
                        Invia
                      </Button>
                    </div>
                  </Form>
                  : <div>Al momento non hai alcuna struttura</div> }
                </Card.Body>
              </Card>
            </div>
          )}
        </Formik>
      </div>
    );
  }
}
