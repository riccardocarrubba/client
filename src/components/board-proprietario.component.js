
import Card from 'react-bootstrap/Card';
import '../css/AreaPersonale.css';
import Form from 'react-bootstrap/Form';
import {FaRegAddressCard, FaHotel, FaBookReader} from 'react-icons/fa';
import React, { Component } from "react";
import "../css/profile.css";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import AuthService from "../services/auth.service";

export default class BoardProprietario extends Component {
                 constructor(props) {
                   super(props);

                   this.state = {
                     content: "",
                     currentUser: AuthService.getCurrentUser()
                   };
                 }



                 render() {
                    const { currentUser } = this.state;
                   return (
                    <div className="container">
                        <div id="profilo">
                            <h1 id="ciao">Ciao, {currentUser.nome}!</h1>
                            <Row>
                                <Col xs="12" sm="12" md="12" lg="6">
                                    <p id="profilo"><strong>Nome: </strong>{currentUser.nome}</p>
                                </Col>
                                <Col xs="12" sm="12" md="12" lg="6">
                                    <p id="profilo"><strong>Cognome: </strong>{currentUser.cognome}</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="12" sm="12" md="12" lg="6">
                                    <p id="profilo"><strong>Data di nascita: </strong>{currentUser.data_nascita}</p>
                                </Col>
                                <Col xs="12" sm="12" md="12" lg="6">
                                    <p id="profilo"><strong>Città: </strong>{currentUser.citta}</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="12" sm="12" md="12" lg="6">
                                    <p id="profilo"><strong>Numero di telefono:</strong>{currentUser.telefono}</p>
                                </Col>
                                <Col xs="12" sm="12" md="12" lg="6">
                                    <p id="profilo"><strong>Email: </strong>{currentUser.mail}</p>
                                </Col>
                            </Row>
                        </div>
                    <Form.Row id="cardproprietario" className="wrapper">
                            <Card style={{ width: '20rem'}} id="datipersonali">
                                <Card.Body id="carddatipersonali">
                                    <Card.Title>
                                        <FaRegAddressCard fontSize={30}/>
                                    </Card.Title>
                                    <Card.Subtitle className="mb-2 text-muted"><Card.Link id="cardproprietariolink" href="/datiPersonali">Modifica dati Personali </Card.Link></Card.Subtitle>
                                    <Card.Text>
                                    Visualizza, modifica i tuoi dati personali o la tua password.
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                            <Card style={{ width: '20rem' }} id="gestionestruttura">
                                <Card.Body id="cardgestionestruttura">
                                    <Card.Title>
                                        <FaHotel fontSize={30} />
                                    </Card.Title>
                                    <Card.Subtitle className="mb-2 text-muted">
                                        <Card.Link id="cardproprietariolink" href="/gestioneStruttura" >Gestione Struttura </Card.Link>
                                    </Card.Subtitle>
                                    <Card.Text>
                                    Visualizza, modifica, aggiungi o elimina le tue Strutture.
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                            <Card style={{ width: '20rem'}} id="gestioneprenotazione">
                                <Card.Body id="cardgestioneprenotazione">
                                    <Card.Title>
                                        <FaBookReader fontSize={30}/>
                                    </Card.Title>
                                    <Card.Subtitle className="mb-2 text-muted"><Card.Link id="cardproprietariolink" href="/gestionePrenotazione">Gestione Prenotazioni</Card.Link></Card.Subtitle>
                                    <Card.Text>
                                    Gestisci le tue prenotazioni: prima che un cliente prenoti una camera della tua struttura puoi accettare o rifiutare la richiesta di prenotazione.
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                    </Form.Row>
                    <Form.Row id="cardproprietario" className="wrapper">

                    </Form.Row>
                </div>
                   );
                 }
               }
