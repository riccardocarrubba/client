import React from "react";
import {Button, Form, Row, Col, Card} from "react-bootstrap";
import * as yup from "yup";
import {Formik, FieldArray} from "formik";
import '../css/FormDatiQuestura.css';
import {MdAddCircleOutline} from "react-icons/md/index";
import struttureService from '../services/strutture.service'
import authService from "../services/auth.service";
import customerService from "../services/customer.service";
import {AiOutlineClose} from 'react-icons/ai';
import {toast,ToastContainer} from "react-toastify";

const validate_birthday = yup
  .string()
  .required("La data di nascita dell'uetnte deve essere immessa")
  .test({
    name: "DOB",
    message: "Deve aver compiuto almeno 18 anni!",
    test: (value) => {
      let now = Date.now();
      let date = Date.parse(value);
      return new Date(now - date).getUTCFullYear() - 1970 >= 18;
    },
  });
const schema = yup.object({
        users: yup.array(
            yup.object({
                nome: yup.string().min(3, 'Il nome è troppo corto').required('Il campo è obbligatorio'),
                cognome:  yup.string().min(3, 'Il cognome è troppo corto').required("Il campo è obbligatorio"),
                citta_nascita: yup.string().min(3, 'La citta è troppo corta').required("Il campo è obbligatorio"),
                citta_residenza: yup.string().min(3, 'La citta è troppo corta').required("Il campo è obbligatorio"),
                data_nascita: validate_birthday,
                check_in: yup.date().required("Il campo è obbligatorio").max(new Date(),"non può essere inserita una data maggiore di quella odierna"),
                check_out: yup.date().required("Il campo è obbligatorio").max(new Date(),"non può essere inserita una data maggiore di quella odierna")
            }))
    });
var DataMassimaNascita;
var DataMassimaOdierna;
export default class FomrRendicondoTrimestrale extends React.Component {
    constructor(props) {
        super(props)
        this.loadData();
        this.getDate();
    }
    userModel = {
        nome: '',
        cognome: '',
        citta_nascita: '',
        citta_residenza: '',
        data_nascita: '',
        check_in: '',
        check_out: ''
    }

    state = {
        structure: 0,
        users: [ this.userModel ],
        listStructures : [],
    }

    loadData = async () => {
      try{
        let user = authService.getCurrentUser()
        let response = await struttureService.getAllStructures(user.id)
        this.setState({listStructures: response.data })
    } catch(e){
    }
  }

     getDate = () => {
      var d = new Date(),
        month = "" + (d.getMonth() + 1),
        day = "" + d.getDate(),
        year = d.getFullYear() - 18,
        ThisYear = d.getFullYear();
      if (month.length < 2) month = "0" + month;
      if (day.length < 2) day = "0" + day;
      this.DataMassimaNascita= `${[year, month, day].join("-")}`;
      this.DataMassimaOdierna = `${[ThisYear, month, day].join("-")}`;
    }
    render() {
        return (
          <div className="container" className="allScreen">
            <ToastContainer />
            <Formik
              initialValues={this.state}
              validationSchema={schema}
              onSubmit={async (values, { setSubmitting }) => {
                try {
                  let bool = true;
                  for await (let user of values.users) {
                    let d = new Date(
                      Date.parse(user.check_out + "T00:00:00.000Z")
                    ).getTime();
                    let d2 = new Date(
                      Date.parse(user.check_in + "T00:00:00.000Z")
                    ).getTime();
                    if (d < d2) {
                      bool = false;
                    }
                  }

                  if (bool == true) {
                    let data = {
                      utenti: values.users,
                      struttura: this.state.listStructures[values.structure],
                    };
                    let response = await customerService.sendThreeMonthReport(
                      data
                    );
                    toast("I tuoi dati sono stati inviati correttamente");
                    setTimeout(() => {
                      window.location.reload();
                    }, 1000);
                  } else {
                    toast("il check-in non può essere maggiore del check-out");
                  }
                } catch (e) {}
                setSubmitting(false);
              }}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
              }) => (
                <div>
                  <Card>
                    <Card.Header>Rendiconto Trimestrale</Card.Header>
                    <Card.Body>
                      {this.state.listStructures.length > 0  ?
                      <Form onSubmit={handleSubmit}>
                        <div>
                          <Form.Group controlId="structure">
                            <Form.Label>Seleziona la struttura</Form.Label>
                            <Form.Control
                              as="select"
                              name="structure"
                              value={values.structure}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            >
                              {this.state.listStructures.map(
                                (struct, index) => (
                                  <option key={"sel" + index} value={index}>
                                    {this.state.listStructures[index].nome}
                                  </option>
                                )
                              )}
                            </Form.Control>
                          </Form.Group>
                          <Form.Group>
                            <Row>
                              <Col>Tassa di soggiorno:</Col>
                              <Col xs={9}>
                                {values.structure !== undefined ? (
                                  <Form.Control
                                    type="text"
                                    placeholder={
                                      this.state.listStructures.length > 0
                                        ? this.state.listStructures[
                                            values.structure
                                          ].tassa
                                        : null
                                    }
                                    readOnly
                                  />
                                ) : null}
                              </Col>
                            </Row>
                          </Form.Group>
                        </div>
                        <FieldArray
                          name="users"
                          render={(arrayHelper) => (
                            <div>
                              {values.users.map((user, index) => (
                                <div
                                  className="padding-row"
                                  key={"user" + index}
                                >
                                  <Form.Row>
                                    <Form.Group controlId="nome">
                                      <Form.Label>Nome:</Form.Label>
                                      <Form.Control
                                        name={`users.${index}.nome`}
                                        type="text"
                                        value={user.nome}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        size="lg"
                                        isInvalid={
                                          errors && touched
                                            ? errors.users && touched.users
                                              ? errors.users[index] &&
                                                touched.users[index]
                                                ? errors.users[index].nome &&
                                                  touched.users[index].nome
                                                : null
                                              : null
                                            : null
                                        }
                                      />
                                      <Form.Control.Feedback
                                        className="FeedBack"
                                        type="invalid"
                                      >
                                        {errors
                                          ? errors.users
                                            ? errors.users[index]
                                              ? errors.users[index].nome
                                                ? errors.users[index].nome
                                                : null
                                              : null
                                            : null
                                          : null}
                                      </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group controlId="cognome">
                                      <Form.Label>Cognome:</Form.Label>
                                      <Form.Control
                                        name={`users.${index}.cognome`}
                                        type="text"
                                        value={user.cognome}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        size="lg"
                                        isInvalid={
                                          errors && touched
                                            ? errors.users && touched.users
                                              ? errors.users[index] &&
                                                touched.users[index]
                                                ? errors.users[index].cognome &&
                                                  touched.users[index].cognome
                                                : null
                                              : null
                                            : null
                                        }
                                      />
                                      <Form.Control.Feedback
                                        className="FeedBack"
                                        type="invalid"
                                      >
                                        {errors
                                          ? errors.users
                                            ? errors.users[index]
                                              ? errors.users[index].cognome
                                                ? errors.users[index].cognome
                                                : null
                                              : null
                                            : null
                                          : null}
                                      </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group controlId="data_nascita">
                                      <Form.Label>Data di nascita:</Form.Label>
                                      <Form.Control
                                        size="lg"
                                        type="date"
                                        max={this.DataMassimaNascita}
                                        name={`users.${index}.data_nascita`}
                                        value={user.data_nascita}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isInvalid={
                                          errors && touched
                                            ? errors.users && touched.users
                                              ? errors.users[index] &&
                                                touched.users[index]
                                                ? errors.users[index]
                                                    .data_nascita &&
                                                  touched.users[index]
                                                    .data_nascita
                                                : null
                                              : null
                                            : null
                                        }
                                      />
                                      <Form.Control.Feedback
                                        className="FeedBack"
                                        type="invalid"
                                      >
                                        {errors
                                          ? errors.users
                                            ? errors.users[index]
                                              ? errors.users[index].data_nascita
                                                ? errors.users[index]
                                                    .data_nascita
                                                : null
                                              : null
                                            : null
                                          : null}
                                      </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group controlId="citta_nascita">
                                      <Form.Label>Città di nascita:</Form.Label>
                                      <Form.Control
                                        name={`users.${index}.citta_nascita`}
                                        type="text"
                                        value={user.citta_nascita}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        size="lg"
                                        isInvalid={
                                          errors && touched
                                            ? errors.users && touched.users
                                              ? errors.users[index] &&
                                                touched.users[index]
                                                ? errors.users[index]
                                                    .citta_nascita &&
                                                  touched.users[index]
                                                    .citta_nascita
                                                : null
                                              : null
                                            : null
                                        }
                                      />
                                      <Form.Control.Feedback
                                        className="FeedBack"
                                        type="invalid"
                                      >
                                        {errors
                                          ? errors.users
                                            ? errors.users[index]
                                              ? errors.users[index]
                                                  .citta_nascita
                                                ? errors.users[index]
                                                    .citta_nascita
                                                : null
                                              : null
                                            : null
                                          : null}
                                      </Form.Control.Feedback>
                                    </Form.Group>
                                  </Form.Row>
                                  <Form.Row>
                                    <Form.Group controlId="citta_residenza">
                                      <Form.Label>
                                        Città di residenza:
                                      </Form.Label>
                                      <Form.Control
                                        name={`users.${index}.citta_residenza`}
                                        type="text"
                                        value={user.citta_residenza}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        size="lg"
                                        isInvalid={
                                          errors && touched
                                            ? errors.users && touched.users
                                              ? errors.users[index] &&
                                                touched.users[index]
                                                ? errors.users[index]
                                                    .citta_residenza &&
                                                  touched.users[index]
                                                    .citta_residenza
                                                : null
                                              : null
                                            : null
                                        }
                                      />
                                      <Form.Control.Feedback
                                        className="FeedBack"
                                        type="invalid"
                                      >
                                        {errors
                                          ? errors.users
                                            ? errors.users[index]
                                              ? errors.users[index]
                                                  .citta_residenza
                                                ? errors.users[index]
                                                    .citta_residenza
                                                : null
                                              : null
                                            : null
                                          : null}
                                      </Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group controlId="check_in">
                                      <Form.Label>Check-in</Form.Label>
                                      <Form.Control
                                        size="lg"
                                        type="date"
                                        max={this.DataMassimaOdierna}
                                        name={`users.${index}.check_in`}
                                        value={user.check_in}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isInvalid={
                                          errors && touched
                                            ? errors.users && touched.users
                                              ? errors.users[index] &&
                                                touched.users[index]
                                                ? errors.users[index]
                                                    .check_in &&
                                                  touched.users[index].check_in
                                                : null
                                              : null
                                            : null
                                        }
                                      />
                                      <Form.Control.Feedback
                                        className="FeedBack"
                                        type="invalid"
                                      >
                                        {errors
                                          ? errors.users
                                            ? errors.users[index]
                                              ? errors.users[index].check_in
                                                ? errors.users[index].check_in
                                                : null
                                              : null
                                            : null
                                          : null}
                                      </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group controlId="check_out">
                                      <Form.Label>Check-out:</Form.Label>
                                      <Form.Control
                                        size="lg"
                                        type="date"
                                        max={this.DataMassimaOdierna}
                                        name={`users.${index}.check_out`}
                                        value={user.check_out}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isInvalid={
                                          errors && touched
                                            ? errors.users && touched.users
                                              ? errors.users[index] &&
                                                touched.users[index]
                                                ? errors.users[index]
                                                    .check_out &&
                                                  touched.users[index].check_out
                                                : null
                                              : null
                                            : null
                                        }
                                      />
                                      <Form.Control.Feedback
                                        className="FeedBack"
                                        type="invalid"
                                      >
                                        {errors
                                          ? errors.users
                                            ? errors.users[index]
                                              ? errors.users[index].check_out
                                                ? errors.users[index].check_out
                                                : null
                                              : null
                                            : null
                                          : null}
                                      </Form.Control.Feedback>
                                    </Form.Group>
                                    <Button
                                      type="button"
                                      id="rendiconto-button"
                                      variant="danger small-button"
                                      onClick={() =>
                                        values.users.length > 1
                                          ? arrayHelper.remove(index)
                                          : null
                                      }
                                    >
                                      <AiOutlineClose />
                                    </Button>
                                  </Form.Row>
                                  <hr />
                                </div>
                              ))}

                              <Row>
                                <Button
                                  variant="primary"
                                  type="button"
                                  onClick={() =>
                                    arrayHelper.push(this.userModel)
                                  }
                                >
                                  Aggiungi cliente <MdAddCircleOutline />
                                </Button>
                              </Row>

                              <div className="row-margin">
                                <Button
                                  variant="primary"
                                  type="submit"
                                  className="float-right"
                                >
                                  Invia
                                </Button>
                              </div>
                            </div>
                          )}
                        />
                      </Form>
                      : <div>Al momento non hai alcuna struttura</div> }
                    </Card.Body>
                  </Card>
                </div>
              )}
            </Formik>
          </div>
        );
    }
}
