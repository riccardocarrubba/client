import React from "react";
import '../css/GestionePrenotazioni.css';
import bookService from '../services/book.service'
import authService from "../services/auth.service";
import Table from 'react-bootstrap/Table';

export default class PrenotazioniEffettuate extends React.Component {
    state = {
        listReservation: [],
        show: false,
        index: undefined
    }
    constructor(props) {
        super(props)
        this.loadData()
    }
    loadData = async () => {
        try {
            let user = authService.getCurrentUser()
            let response = await bookService.allBooksCliente(user.id)
            this.setState({listReservation: response.data})
            this.handleShow(false)
        } catch (e) {
        }
    }

    handleShow = (state) => {this.setState({show: state})}



    render() {
        return (
            <div className="container">
                        <div id="tabella-prenotazionieffettuate">
                                    <Table responsive>
                                            <thead>
                                                <tr>
                                                <th>ID_BOOKING</th>
                                                <th>CHECK-IN</th>
                                                <th>CHECK-OUT</th>
                                                <th>PREZZO</th>
                                                <th>NOME STRUTTURA</th>
                                                <th>NOME CAMERA</th>
                                                <th>TASSA</th>
                                                <th>STATO</th>
                                                </tr>
                                            </thead>
                                            {this.state.listReservation.map((prenotazione, index) =>
                                            <tbody  key= {index}>
                                                <tr>
                                                <td>{prenotazione.id}</td>
                                                <td>{prenotazione.check_in}</td>
                                                <td>{prenotazione.check_out}</td>
                                                <td>{prenotazione.prezzo}</td>
                                                <td>{prenotazione.nome_struttura}</td>
                                                <td>{prenotazione.nome_camera ? prenotazione.nome_camera : "CASA VACANZA"}</td>
                                                <td>{prenotazione.tassa ?  'In loco' :'Online' }</td>
                                                <td>{prenotazione.attesa ? 'In attessa' : 'Confermata'}</td>
                                                </tr>


                                            </tbody>
                                            )}
                                            </Table>

                                 </div>




            </div>
        );
    }
};
