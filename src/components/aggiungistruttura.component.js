import React from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import * as yup from "yup";
import {Formik} from "formik";
import Card from "react-bootstrap/Card";
import InputGroup from "react-bootstrap/InputGroup";
import AuthService from "../services/auth.service";
import StructuresService from "../services/strutture.service";
import {ToastContainer, toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {Col, Row} from "react-bootstrap";
import '../css/GestioneStruttura.css';

const schema = yup.object().shape({
    nome: yup.string().required("Per favore, inserisci il nome della struttura."),
    citta: yup.string().required("Inserire una città valida"),
    indirizzo: yup.string().required("Inserisci il tuo indirizzo"),
    tassa: yup.string().required("Inserisci la tassa di soggiorno").matches(/^[0-9]\d*(\.\d+)?$/, "La tassa inserita non può essere negativa"),
    cap: yup.string().required("Inserisci il tuo CAP").matches(/^[0-9]{5,5}$/,"Il CAP deve contenere 5 interi."),
    telefono: yup.string().matches(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/, "Deve contenere 10 interi."),
    descrizione: yup.string().required("Inserisci la descrizione della struttura (es: luoghi d'interesse vicini alla struttura)"),
    prezzo_req: yup
        .string()
        .required("Inserisci il prezzo")
        .matches(/^(?=.*[0-9])(?=.{1,})/, "Deve contenere numeri."),
});

export default class FormAggiungiStruttura extends React.Component {
  state = {
    file: "",
    prezzo: "",
    posti_letto: 2,
    checkFiles:true,
  };

    handlerChange = async (event) => {
       let files = event.target.files;
       let checkFiles=false;
        for await (let file of files){
            let filename =file.name.split(".").pop();
            let filenamee = filename.toLowerCase()
             if(filenamee !== 'png' && filenamee !== 'jpg' && filenamee !== 'jpeg'){
                toast("si possono caricare solo immagini");
                checkFiles=true;
                files =[];
                break;
             }
        }
        this.setState({ file: files, checkFiles: checkFiles });
  };

  registraStruttura = async (obj) => {
    try {
      let response = await StructuresService.registraStruttura(obj);
      let data = new FormData();
      for (let i = 0; i < this.state.file.length; i++) {
        data.append("images", this.state.file[i]);
      }
      data.append("anotherdata", response.data.id);
      let res = await StructuresService.aggiungiImmagine(data);
      this.props.history.push("/gestioneStruttura");
    } catch (e) {
      toast(e.response.data);
    }
  };

  handleChangePrezzo = (event) => {
    this.setState({ prezzo: event.target.value });
  };

  handleChangeOspiti = (event) => {
    this.setState({ posti_letto: event.target.value });
  };

  calcolaPrezzo = (values) => {
    if (values.tipologia == 1) {
      this.setState({ prezzo: 0 });
      this.setState({ posti_letto: 0 });
    } else if (values.tipologia == 2) {
      this.setState({ prezzo: values.prezzo_req });
    }
  };

  render() {
    return (
      <div className="container">
        <Formik
          validationSchema={schema}
          onSubmit={async (values, { setSubmitting }) => {

            this.calcolaPrezzo(values);
            let _obj = { ...values };
            _obj.prezzo = this.state.prezzo;
            _obj.posti_letto = this.state.posti_letto;
            delete _obj.prezzo_req;

            this.registraStruttura(_obj);
            setSubmitting(false);
          }}
          initialValues={{
            nome: "",
            descrizione: "",
            citta: "",
            indirizzo: "",
            tassa: "",
            cap: "",
            prezzo_req: 40,
            tipologia: 1, //"Bed & Breakfast"
            telefono: "",
            wifi: false,
            parcheggio: false,
            ascensore_disabili: false,
            utente_id: AuthService.getCurrentUser().id,
          }}
        >
          {({
            handleSubmit,
            handleChange,
            handleBlur,
            values,
            touched,
            isValid,
            errors,
            isSubmitting,
          }) => (
            <div className="Container" id="registrazione">
              <div className="wrapper">
                <ToastContainer />
                <Card id="registrazionecard">
                  <Card.Header id="cardheaderregistrazione">
                    Aggiungi Struttura
                  </Card.Header>
                  <Card.Body>
                    <Form
                      noValidate
                      onSubmit={handleSubmit}
                      id="formregistrazione"
                    >
                      <Form.Row>
                        <Form.Group controlId="formBasicNome">
                          <label>Nome struttura</label>
                          <Form.Control
                            name="nome"
                            type="text"
                            lable="Nome"
                            placeholder="Nome Struttura"
                            value={values.nome}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.nome && errors.nome ? "error" : null
                            }
                            className="SignUpFormControls"
                            size="lg"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.nome}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="formBasicCitta">
                          <label>Città</label>
                          <Form.Control
                            name="citta"
                            type="text"
                            placeholder="Città"
                            value={values.citta}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.citta && errors.citta ? "error" : null
                            }
                            className="SignUpFormControls"
                            size="lg"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.citta}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="formBasicIndirizzo">
                          <label>Indirizzo</label>
                          <Form.Control
                            size="lg"
                            className="SignUpFormControls"
                            type="text"
                            lable="Indirizzo"
                            name="indirizzo"
                            value={values.indirizzo}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.indirizzo && errors.indirizzo
                                ? "error"
                                : null
                            }
                            placeholder="Indirizzo"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.indirizzo}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Form.Row>
                      <Form.Row>
                        <Form.Group controlId="formBasicTassa">
                          <label>Tassa di soggiorno</label>
                          <Form.Control
                            size="lg"
                            placeholder="Tassa di soggiorno"
                            className="SignUpFormControls"
                            type="number"
                            step="0.01"
                            pattern="^\d+(?:\.\d{1,2})?$"
                            min="0"
                            name="tassa"
                            lable="Tassa di soggiorno"
                            value={values.tassa}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.tassa && errors.tassa ? "error" : null
                            }
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.tassa}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="formBasicCap">
                          <label>CAP:</label>
                          <Form.Control
                            size="lg"
                            className="SignUpFormControls"
                            type="text"
                            name="cap"
                            lable="CAP"
                            value={values.cap}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.cap && errors.cap ? "error" : null
                            }
                            placeholder="CAP"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.cap}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="formBasicTelefono">
                          <label>Numero di telefono:</label>
                          <Form.Control
                            type="text"
                            name="telefono"
                            lable="Numero di telefono"
                            placeholder="Telefono"
                            value={values.telefono}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.telefono && errors.telefono
                                ? "error"
                                : null
                            }
                            className="SignUpFormControls"
                            size="lg"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.telefono}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Form.Row>

                      <Form.Row>
                        <Form.Group id="file">
                          <Form.File
                            className="position-relative"
                            required
                            name="file"
                            multiple="multiple"
                            accept=".jpg,.png,.jpeg"
                            onChange={this.handlerChange}
                            id="validationFormik107"
                          />
                        </Form.Group>
                      </Form.Row>
                      <Form.Row>
                        <InputGroup>
                          <InputGroup.Prepend>
                            <InputGroup.Text>Descrizione</InputGroup.Text>
                          </InputGroup.Prepend>
                          <Form.Control
                            as="textarea"
                            name="descrizione"
                            size="lg"
                            className="SignUpFormControls"
                            type="text"
                            value={values.descrizione}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.descrizione && errors.descrizione
                                ? "error"
                                : null
                            }
                            placeholder=""
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.descrizione}
                          </Form.Control.Feedback>
                        </InputGroup>
                      </Form.Row>

                      <Form.Row>
                        {values.tipologia === "2" ? (
                          <Form.Row>
                            <Col xs="12" sm="12" md="6" lg="6">
                              <label>Prezzo</label>
                              <InputGroup>
                                <InputGroup.Prepend>
                                  <InputGroup.Text id="prezzo">
                                    €
                                  </InputGroup.Text>
                                </InputGroup.Prepend>

                                <Form.Control
                                  type="text"
                                  placeholder=""
                                  lable="Prezzo"
                                  aria-describedby="inputGroupPrepend"
                                  name="prezzo_req"
                                  value={values.prezzo_req}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  isInvalid={
                                    touched.prezzo_req && errors.prezzo_req
                                      ? "error"
                                      : null
                                  }
                                />
                              </InputGroup>
                            </Col>
                            <Col xs="12" sm="12" md="6" lg="6">
                              <label>Posti letto</label>
                              <Form.Control
                                as="select"
                                name="posti_letto"
                                lable="Posti letto"
                                value={values.posti_letto}
                                onChange={this.handleChangeOspiti}
                                onBlur={handleBlur}
                              >
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                              </Form.Control>
                            </Col>
                          </Form.Row>
                        ) : null}
                      </Form.Row>

                      <Row>
                        <div className="specifiche">
                          <fieldset>
                            <Form.Group id="tipologia">
                              <Form.Label as="legend">
                                Tipo di alloggio:
                              </Form.Label>
                              <Form.Control
                                as="select"
                                value={values.tipologia}
                                name="tipologia"
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <option value="1">Bed & Breakfast</option>
                                <option value="2">Casa Vacanza</option>
                              </Form.Control>
                            </Form.Group>
                          </fieldset>
                        </div>

                        <div className="specifiche">
                          <Form.Group
                            id="servizi"
                            onChange={handleChange}
                            onBlur={handleBlur}
                          >
                            <Form.Label as="legend">Servizi:</Form.Label>
                            <Form.Check label="Wi-Fi" id="wifi" />
                            <Form.Check
                              label="Parcheggio"
                              id="parcheggio"
                              name="parcheggio"
                              value={values.parcheggio}
                            />
                            <Form.Check
                              label="Ascensore Disabili"
                              id="ascensore_disabili"
                              name="ascensore_disabili"
                              value={values.ascensore_disabili}
                            />
                          </Form.Group>
                        </div>
                      </Row>
                      <Form.Row className="top-10">
                        <Button variant="primary" type="submit" disabled={this.state.checkFiles ? true : false}>
                          Registra
                        </Button>
                      </Form.Row>
                    </Form>
                  </Card.Body>
                </Card>
              </div>
            </div>
          )}
        </Formik>
      </div>
    );
  }
}

