import React from "react";
import {Button, Card, Col, Row, Carousel} from "react-bootstrap";
import '../css/infoStruttura.css';
import '../css/GestioneStruttura.css';
import cameraService from '../services/camere.service'
import {Config} from '../config/config'
import {MdAddCircleOutline} from 'react-icons/md';
import {ToastContainer, toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {RiDeleteBinLine} from "react-icons/ri/index";
import Modal from 'react-bootstrap/Modal';
import {AiFillPhone, AiOutlineWifi} from 'react-icons/ai';
import {GiLift} from 'react-icons/gi';
import {TiEdit} from 'react-icons/ti';
import {FaParking, FaEye} from 'react-icons/fa';



export default class InfoStrutturaComponent extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            structure: props.location.state,
            rooms: [],
            show: false,
            index: undefined,
            camera_id: undefined
        }
        if (this.state.structure) {
            this.loadData(this.state.structure.id)
        }
    }

    loadData = async (id) => {
        try {
            let response = await cameraService.getRoomsByStructure(id)
            this.setState({rooms: response.data})
        } catch (e) {
        }

    }


    eliminaCamera = async () => {
        try {
            const {index,camera_id} = this.state;
            let response = await cameraService.deleteRooms({"id_camera":camera_id, "path": this.state.rooms[index].path});
            let rooms = this.state.rooms;
            toast("La camera " + rooms[index].nome + " è stata eliminata");
            rooms.splice(index, 1);
            this.setState({room: rooms, show: false})
        } catch (e) {
            toast(e.response.data);
        }
    }


    handleShow = (state) => {this.setState({show: state})}
    render() {
        return (
            <div className="container">
                <ToastContainer />
                {this.state.structure ? <div>
                        <Card  className="card-appearance" id="infostruttura">
                            {this.state.structure.paths.length > 0 ?
                                <Carousel >
                                    {this.state.structure.paths.map((image, index) =>
                                        <Carousel.Item key={`car-`+index}>
                                            <img  id="struttura-img"
                                                className="d-block w-100 "
                                                src={Config.IMAGE_URL + image.path}
                                                alt={index + "slide"}
                                            />
                                        </Carousel.Item>
                                    )}
                                </Carousel>
                                : null}


                            <Card.Body>
                                <Card.Title>{this.state.structure.nome}</Card.Title>

                                <span><strong>Descrizione:</strong>  {this.state.structure.descrizione}  </span>
                                    <li>I servizi della struttura:</li>
                                    <ul>{this.state.structure.wifi ?
                                         <span><AiOutlineWifi fontSize={25}/>Wi-fi</span> : null} </ul>
                                    <ul>{this.state.structure.parcheggio ?
                                        <span><FaParking fontSize={25}/> Parcheggio</span> : null}</ul>
                                    <ul>{this.state.structure.ascensore_disabili ?
                                        <span><GiLift fontSize={25}/> Ascensore</span> : null}</ul>
                                    <span>{this.state.structure.telefono ?
                                        <span>Contatti: <AiFillPhone fontSize={25}/>{this.state.structure.telefono}</span> : null }</span>

                            </Card.Body>
                        </Card>
                    {
                        this.state.structure.tipologia === 1 ?
                        <Row>
                        <Col xs="12" sm="6" md="4" lg="3" style={{ width: '10rem', marginTop: "20px", marginBottom: "30px"}}>
                            <Card id="aggiugiCamera" onClick={() => this.props.history.push("/aggiungiCamera", this.state.structure)}>
                                <Card.Body id="cardaggiugiCamera">
                                    <Card.Title>Aggiungi camera</Card.Title>

                                    <Card.Title>
                                        <MdAddCircleOutline fontSize={30}/>
                                    </Card.Title>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    : null
                    }

                    {this.state.rooms.map((room, index) =>
                                <Row className="lista-camere" key={'room'+index}>
                                    <Col className="immagine" xs="12" sm="4" md="4" lg="5">
                                        <img variant="top" className="card-img" src={Config.IMAGE_URL +room.path} />
                                    </Col>
                                    <Col className="descrizione" xs="12" sm="5" md="5" lg="5">
                                        <Row>
                                            <span><strong fontSize={20}>{room.nome}</strong></span>
                                        </Row>
                                        <Row>
                                            <p> {room.descrizione} </p>
                                        </Row>
                                    </Col>
                                    <Col xs="6" sm="3" md="3" lg="2">
                                        <Row>
                                            <Button className="card-struttura-button" variant="primary small-button" type="button"
                                                onClick={() => {this.props.history.push("/visualizzaCamera", room)}}><FaEye  fontSize={23}/>
                                            </Button>
                                        </Row>
                                        <Row>
                                            <Button className="card-struttura-button" variant="primary small-button" type="button"
                                            onClick={() => this.props.history.push("/modificaCamera", room)}><TiEdit fontSize={23} />
                                            </Button>
                                        </Row>
                                        <Row>
                                            <Button className="card-struttura-button"  variant="danger small-button" type="button"
                                                        onClick={() => { this.handleShow(true)} }
                                                        onMouseDown={() => {this.setState({index: index,camera_id: room.id})}}>
                                                    <RiDeleteBinLine fontSize={23}/>
                                            </Button>


                                        </Row>

                                    </Col>
                                </Row>
                            )}
                            <Modal show={this.state.show} onHide={() => this.handleShow(false)}>
                                                    <Modal.Header closeButton>
                                                        <Modal.Title>Attenzione</Modal.Title>
                                                    </Modal.Header>
                                                    <Modal.Body>Sei sicuro di voler eliminare questa camera?</Modal.Body>
                                                    <Modal.Footer>
                                                        <Button variant="primary small-button" onClick={() => { this.handleShow(false)} } >
                                                            Chiudi
                                                        </Button>
                                                        <Button  variant="danger small-button" onClick={() => {this.eliminaCamera()}}>
                                                            Elimina
                                                        </Button>
                                                    </Modal.Footer>
                                                </Modal>
                    </div>
                    :
                    <div> nessuna struttura selezionata </div>}
            </div>
        );
    }
};
