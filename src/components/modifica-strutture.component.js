import React from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import * as yup from "yup";
import { Formik } from "formik";
import Card from "react-bootstrap/Card";
import StructuresService from "../services/strutture.service";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Row } from "react-bootstrap";
import InputGroup from "react-bootstrap/InputGroup";
import "../css/GestioneStruttura.css";

const schema = yup.object().shape({
  nome: yup.string().required("Per favore, inserisci il nome della struttura."),
  citta: yup.string().required("Inserire una città valida"),
  indirizzo: yup.string().required("Inserisci il tuo indirizzo"),
  tassa: yup.string().required("Inserisci la tassa di soggiorno").matches(/^[0-9]\d*(\.\d+)?$/, "La tassa inserita non può essere negativa"),
  cap: yup.string().required("Inserisci il tuo CAP").matches(/^[0-9]{5,5}$/,"Il CAP deve contenere 5 interi."),
  telefono: yup.string().matches(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/, "Deve contenere 10 interi."),
  descrizione: yup.string().required("Inserisci la descrizione della struttura (es: luoghi d'interesse vicini alla struttura)"),
  prezzo: yup.string().required("Inserisci il prezzo della camera").matches(/^[0-9]\d*(\.\d+)?$/, "Il prezzo inserito non può essere negativo"),

});

export default class ModificaStrutture extends React.Component {
  constructor(props) {
    super(props);
    try {
      this.state = {
        strutture: props.location.state,
        show: false,
        file: [],
        loaded: 0,
        checkFiles: false,
      };
    } catch (e) {
      this.state = {
        strutture: null,
        show: false,
        file: [],
        loaded: 0,
      };
    }
  }
  handleShow = (state) => {
    this.setState({ show: state });
  };

  aggiornaFoto = async () => {
    try {
      let obj = {"id_struttura" : this.state.strutture.id, "path" : this.state.strutture.paths }
      let response = await StructuresService.deleteImages(
        obj
      );
      let data = new FormData();
      for (let i = 0; i < this.state.file.length; i++) {
        data.append("images", this.state.file[i]);
      }
      data.append("anotherdata", this.state.strutture.id);
      let res = await StructuresService.aggiungiImmagine(data);
      let path = [];
      for await (let file of res.data.files) {
        path.push(file.filename);
      }
      this.setState({ strutture: path });
      await toast("Le immagini sono state eliminate con successo");
    } catch (e) {
      toast(e.response.data);
    }
  };

      handlerChange = async (event) => {
       let files = event.target.files;
       let checkFiles=false;
        for await (let file of files){
            let filename =file.name.split(".").pop();
            let filenamee = filename.toLowerCase()
             if(filenamee !== 'png' && filenamee !== 'jpg' && filenamee !== 'gif' && filenamee !== 'jpeg'){
                toast("si possono caricare solo immagini");
                checkFiles=true;
                files =[];
                break;
             }
        }
        this.setState({ file: files, checkFiles: checkFiles });
  };
  handleChangeServizi = (e) => {
    const wifi = document.getElementById("wifi");
    const parcheggio = document.getElementById("parcheggio");
    const ascensore = document.getElementById("ascensore_disabili");
    let str = this.state.strutture;
    str.wifi = wifi.checked;
    str.parcheggio = parcheggio.checked;
    str.ascensore = ascensore.checked;
    this.setState({strutture: str})
  };

  render() {
    return (
      <div className="container">
        {this.state.strutture !== null ? (
          <Formik
            initialValues={this.state.strutture}
            validationSchema={schema}
            onSubmit={async (values) => {
              try {
                if (this.state.file.length !== 0) {
                  await this.aggiornaFoto();
                }
                let response = await StructuresService.modifyStructures(values);
                if(response.data.updated !==undefined){
                  await toast("I campi sono uguali a quelli precedenti");
                  setTimeout(() => {
                    this.props.history.goBack();
                  }, 700)
                }else{
                  await toast("la struttura è stata correttamente aggiornata");
                  setTimeout(() => {
                      this.props.history.goBack();
                  }, 700)
                }
              } catch (e) {
                toast(e.response.data);
              }
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
            }) => (
              <div className="Container" id="registrazione">
                <div className="wrapper">
                  <ToastContainer />
                  <Card id="registrazionecard">
                    <Card.Header id="cardheaderregistrazione">
                      Modifica Struttura
                    </Card.Header>
                    <Card.Body>
                      <Form onSubmit={handleSubmit} id="formregistrazione">
                        <Form.Row>
                          <Form.Group controlId="formBasicNome">
                              <Form.Label>Nome:</Form.Label>
                            <Form.Control
                              name="nome"
                              type="text"
                              placeholder="Nome Struttura"
                              value={values.nome}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              isInvalid={
                                touched.nome && errors.nome ? "error" : null
                              }
                              className="SignUpFormControls"
                              size="lg"
                            />
                            <Form.Control.Feedback
                              className="FeedBack"
                              type="invalid"
                            >
                              {errors.nome}
                            </Form.Control.Feedback>
                          </Form.Group>
                          <Form.Group controlId="formBasicCitta">
                              <Form.Label>Città:</Form.Label>
                            <Form.Control
                              name="citta"
                              type="text"
                              placeholder="Città"
                              value={values.citta}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              isInvalid={
                                touched.citta && errors.citta ? "error" : null
                              }
                              className="SignUpFormControls"
                              size="lg"
                            />
                            <Form.Control.Feedback
                              className="FeedBack"
                              type="invalid"
                            >
                              {errors.citta}
                            </Form.Control.Feedback>
                          </Form.Group>
                          <Form.Group controlId="formBasicIndirizzo">
                              <Form.Label>Indirizzo:</Form.Label>
                            <Form.Control
                              size="lg"
                              className="SignUpFormControls"
                              type="text"
                              name="indirizzo"
                              value={values.indirizzo}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              isInvalid={
                                touched.indirizzo && errors.indirizzo
                                  ? "error"
                                  : null
                              }
                              placeholder="Indirizzo"
                            />
                            <Form.Control.Feedback
                              className="FeedBack"
                              type="invalid"
                            >
                              {errors.indirizzo}
                            </Form.Control.Feedback>
                          </Form.Group>
                        </Form.Row>
                        <Form.Row>
                          <Form.Group controlId="formBasicTassa">
                              <Form.Label>Tassa:</Form.Label>
                            <Form.Control
                              size="lg"
                              placeholder="Tassa di soggiorno"
                              className="SignUpFormControls"
                              type="number"
                              min="0"
                              step="0.01"
                              pattern="^\d+(?:\.\d{1,2})?$"
                              name="tassa"
                              value={values.tassa}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              isInvalid={
                                touched.tassa && errors.tassa ? "error" : null
                              }
                            />
                            <Form.Control.Feedback
                              className="FeedBack"
                              type="invalid"
                            >
                              {errors.tassa}
                            </Form.Control.Feedback>
                          </Form.Group>
                          <Form.Group controlId="formBasicCap">
                              <Form.Label>Cap:</Form.Label>
                            <Form.Control
                              size="lg"
                              className="SignUpFormControls"
                              type="text"
                              name="cap"
                              value={values.cap}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              isInvalid={
                                touched.cap && errors.cap ? "error" : null
                              }
                              placeholder="CAP"
                            />
                            <Form.Control.Feedback
                              className="FeedBack"
                              type="invalid"
                            >
                              {errors.cap}
                            </Form.Control.Feedback>
                          </Form.Group>
                          <Form.Group controlId="formBasicTelefono">
                              <Form.Label>Telefono:</Form.Label>
                            <Form.Control
                              type="text"
                              name="telefono"
                              placeholder="Telefono"
                              value={values.telefono}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              isInvalid={
                                touched.telefono && errors.telefono
                                  ? "error"
                                  : null
                              }
                              className="SignUpFormControls"
                              size="lg"
                            />
                            <Form.Control.Feedback
                              className="FeedBack"
                              type="invalid"
                            >
                              {errors.telefono}
                            </Form.Control.Feedback>
                          </Form.Group>
                          {this.state.strutture.tipologia == 2 ? (
                            <InputGroup>
                              <InputGroup.Prepend>
                                <InputGroup.Text id="prezzo">€</InputGroup.Text>
                              </InputGroup.Prepend>
                              <Form.Control
                                placeholder="Prezzo"
                                aria-describedby="inputGroupPrepend"
                                name="prezzo"
                                type="number"
                                step="0.01"
                                pattern="^\d+(?:\.\d{1,2})?$"
                                min="0"
                                value={values.prezzo}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isInvalid={
                                  touched.prezzo && errors.prezzo
                                    ? "error"
                                    : null
                                }
                              />
                              <Form.Control.Feedback type="invalid" tooltip>
                                {errors.prezzo}
                              </Form.Control.Feedback>
                            </InputGroup>
                          ) : null}
                        </Form.Row>

                        <Form.Row>
                          <Form.Group id="file">
                            <Form.File
                              className="position-relative"
                              name="file"
                              accept=".jpg,.png,.jpeg"
                              multiple="multiple"
                              onChange={this.handlerChange}
                              id="validationFormik107"
                            />
                          </Form.Group>
                        </Form.Row>
                        <Form.Row>
                          <InputGroup>
                            <InputGroup.Prepend>
                              <InputGroup.Text>Descrizione</InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                              as="textarea"
                              name="descrizione"
                              size="lg"
                              className="SignUpFormControls"
                              type="text"
                              value={values.descrizione}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              isInvalid={
                                touched.descrizione && errors.descrizione
                                  ? "error"
                                  : null
                              }
                              placeholder=""
                            />
                            <Form.Control.Feedback
                              className="FeedBack"
                              type="invalid"
                            >
                              {errors.descrizione}
                            </Form.Control.Feedback>
                          </InputGroup>
                        </Form.Row>

                        <Row>
                          <div className="specifiche">
                            <Form.Group id="tipologia">
                              <Form.Label as="legend">
                                Tipo di alloggio:
                              </Form.Label>
                              <Form.Control
                                as="select"
                                disabled
                                value={values.tipologia}
                                name="tipologia"
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <option value="1">Bed and Breakfast</option>
                                <option value="2">Casa Vacanza</option>
                              </Form.Control>
                            </Form.Group>
                          </div>
                          <div className="specifiche">
                            <Form.Group
                              id="servizi"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              onClick={this.handleChangeServizi}
                            >
                              <Form.Label as="legend">Servizi:</Form.Label>
                              <Form.Check
                                label="Wi-Fi"
                                id="wifi"
                                name="wifi"
                                checked={values.wifi}
                                onChange={() => {}}
                              />
                              <Form.Check
                                label="Parcheggio"
                                id="parcheggio"
                                name="parcheggio"
                                checked={values.parcheggio}
                                onChange={() => {}}
                              />
                              <Form.Check
                                label="Ascensore Disabili"
                                id="ascensore_disabili"
                                name="ascensore_disabili"
                                checked={values.ascensore_disabili}
                                onChange={() => {}}
                              />
                            </Form.Group>
                          </div>
                        </Row>
                        <Form.Row className="top-10">
                          <Button variant="primary" type="submit" disabled={this.state.checkFiles===true ? true : false}>
                            Salva
                          </Button>
                        </Form.Row>
                      </Form>
                    </Card.Body>
                  </Card>
                </div>
              </div>
            )}
          </Formik>
        ) : (
          <div> nessuna struttura selezionata </div>
        )}
      </div>
    );
  }
}
