import React from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import * as yup from "yup";
import { Formik } from "formik";
import Card from "react-bootstrap/Card";
import AuthService from "../services/auth.service";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import '../css/style.css';
import '../css/Autenticazione.css';

const validate_birthday = yup.string().required("La data deve essere immessa")
  .max(new Date(), "non può essere inserita una data maggiore di quella odierna")
  .test({
    name: "DOB",
    message: "Devi aver compiuto almeno 18 anni!",
    test: (value) => {
      let now = Date.now();
      let date = Date.parse(value);
      return new Date(now - date).getUTCFullYear() - 1970 >= 18;
    },
  });

const schema = yup.object({
  nome: yup.string().required("Per favore, inserisci un nome."),
  cognome: yup.string().required("Per favore, inserisci un cognome."),
  data_nascita: validate_birthday,
  mail: yup.string().email().required("Per favore, inserisci la tua email"),
  citta: yup.string().required("Inserire una città valida"),
  confirmEmail: yup.string().email().required("Devi reinserire la tua email").oneOf([yup.ref("mail"), null], "L'email non corrisponde"),
  telefono: yup.string().matches(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/, "Deve contenere 10 interi."),
  cap: yup.string().required("Inserisci il tuo CAP").matches(/^[0-9]{5,5}$/,"Il CAP deve contenere 5 interi."),
  confirmPassword: yup.string().required("Conferma la tua password!").oneOf([yup.ref("password"), null], "La password non corrisponde."),
  password: yup.string().required("Per favore, inserisci una password.").matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\.\-\_!@#\$%\^&\*])(?=.{8,})/, "Deve contenere 8 caratteri, una maiuscola, una minuscola, un numero e un carattere speciale."),
  tipologia: yup.string().required(),
});

var DataMassimaNascita;
export default class AddTutorial extends React.Component {
  constructor(props) {
    super(props);
    this.getDate();
  }
  handleSubmit = async (values) => {
    try {
      let response = await AuthService.register(values);
      await toast(
        `Ciao ${response.data.nome}, registrazione avvenuta con successo`
      );
      this.props.history.push("/login");
    } catch (e) {
      toast(e.response.data);
    }
  };
  getDate = () => {
    var d = new Date(),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear() - 18,
      ThisYear = d.getFullYear();
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;
    this.DataMassimaNascita = `${[year, month, day].join("-")}`;
  };
  render() {
    return (
      <div className="container">
        <Formik
          validationSchema={schema}
          onSubmit={async (values, { setSubmitting }) => {
            this.handleSubmit(values);
            setSubmitting(false);
          }}
          initialValues={{
            nome: "",
            cognome: "",
            citta: "",
            cap: "",
            mail: "",
            confirmEmail: "",
            password: "",
            confirmPassword: "",
            telefono: "",
            data_nascita: "",
            tipologia: false,
          }}
        >
          {({
            handleSubmit,
            handleChange,
            handleBlur,
            values,
            touched,
            isValid,
            dirty,
            errors,
          }) => (
            <div className="Container" id="registrazione">
              <div className="wrapper">
                <ToastContainer />
                <Card id="registrazionecard">
                  <Card.Header id="cardheaderregistrazione">
                    Registrazione
                  </Card.Header>
                  <Card.Body>
                    <Form
                      noValidate
                      onSubmit={handleSubmit}
                      id="formregistrazione"
                    >
                      <Form.Row>
                        <Form.Group controlId="formBasicNome">
                          <Form.Control
                            type="text"
                            name="nome"
                            placeholder="Nome"
                            value={values.nome}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.nome && errors.nome ? "error" : null
                            }
                            className="SignUpFormControls"
                            size="lg"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.nome}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="formBasicCognome">
                          <Form.Control
                            type="text"
                            name="cognome"
                            placeholder="Cognome"
                            value={values.cognome}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.cognome && errors.cognome ? "error" : null
                            }
                            className="SignUpFormControls"
                            size="lg"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.cognome}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="formBasicCitta">
                          <Form.Control
                            type="text"
                            name="citta"
                            placeholder="Città"
                            value={values.citta}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.citta && errors.citta ? "error" : null
                            }
                            className="SignUpFormControls"
                            size="lg"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.citta}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="data_nascita">
                          <Form.Control
                            size="lg"
                            isInvalid={
                              touched.data_nascita && errors.data_nascita
                                ? "error"
                                : null
                            }
                            type="date"
                            name="data_nascita"
                            value={values.data_nascita}
                            max={this.DataMassimaNascita}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Data di nascita"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.data_nascita}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Form.Row>

                      <Form.Row>
                        <Form.Group controlId="telefono">
                          <Form.Control
                            type="text"
                            placeholder="Telefono"
                            value={values.telefono}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="telefono"
                            isInvalid={
                              touched.telefono && errors.telefono
                                ? "error"
                                : null
                            }
                            size="lg"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.telefono}
                          </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="formBasicCap">
                          <Form.Control
                            size="lg"
                            className="SignUpFormControls"
                            type="text"
                            name="cap"
                            value={values.cap}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.cap && errors.cap ? "error" : null
                            }
                            placeholder="CAP"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.cap}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Form.Row>
                      <div>
                        <p id="errorep" style={{ color: "red" }}></p>
                      </div>
                      <Form.Row>
                        <Form.Group controlId="formBasicEmail">
                          <Form.Control
                            name="mail"
                            type="email"
                            placeholder="Email"
                            value={values.mail}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.mail && errors.mail ? "error" : null
                            }
                            className="SignUpFormControls"
                            size="lg"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.mail}
                          </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group controlId="formBasicConfirmEmail">
                          <Form.Control
                            type="email"
                            className="SignUpFormControls"
                            size="lg"
                            name="confirmEmail"
                            value={values.confirmEmail}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.confirmEmail && errors.confirmEmail
                                ? "error"
                                : null
                            }
                            placeholder="Conferma Email"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.confirmEmail}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Form.Row>
                      <Form.Row>
                        <Form.Group controlId="formBasicPassword">
                          <Form.Control
                            className="SignUpFormControls"
                            size="lg"
                            type="password"
                            name="password"
                            value={values.password}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.password && errors.password
                                ? "error"
                                : null
                            }
                            placeholder="Password"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.password}
                          </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group controlId="formBasicConfirmPassword">
                          <Form.Control
                            className="SignUpFormControls"
                            size="lg"
                            name="confirmPassword"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.confirmPassword && errors.confirmPassword
                                ? "error"
                                : null
                            }
                            value={values.confirmPassword}
                            placeholder="Conferma password"
                            type="password"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.confirmPassword}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Form.Row>
                      <Form.Row>
                        <Form.Group controlId="tipologia">
                          <Form.Label>Registrami come:</Form.Label>
                          <Form.Control
                            as="select"
                            name="tipologia"
                            value={values.tipologia}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.tipologia && errors.tipologia
                                ? "error"
                                : null
                            }
                          >
                            <option value="false">cliente</option>
                            <option value="true">proprietario</option>
                          </Form.Control>
                        </Form.Group>
                      </Form.Row>
                      <Form.Row>
                        <Form.Group id="registrati">
                          <Button variant="primary" type="submit">
                            Registrati
                          </Button>
                        </Form.Group>
                      </Form.Row>
                    </Form>
                  </Card.Body>
                </Card>
              </div>
            </div>
          )}
        </Formik>
      </div>
    );
  }
}

