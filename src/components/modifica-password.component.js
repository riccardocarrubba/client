import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import * as yup from "yup";
import { Formik } from "formik";
import Card from "react-bootstrap/Card";
import AuthService from "../services/auth.service";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const schema = yup.object({
    vecchiaPassword: yup.string().required("Immetti la tua vecchia password"),
    confirmPassword: yup.string().required("Conferma la tua password!").oneOf([yup.ref("password"), null], "La password non corrisponde."),
    password: yup.string().required("Per favore, inserisci una password.").matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\.\-\_!@#\$%\^&\*])(?=.{8,})/, "Deve contenere 8 caratteri, una maiuscola, una minuscola, un numero e un carattere speciale."),
})

export default class ModificaPassword extends React.Component {
    handleSubmit = async (values) => {
      try{
          let token = AuthService.getCurrentUser().token
          values.user = AuthService.getCurrentUser()
          let response = await AuthService.passModify(values);
          response.data.token = token
          localStorage.setItem("user", JSON.stringify(response.data))
          await toast(`Ciao ${response.data.nome}, la tua password è stata modificata con successo`);
          setTimeout(() => {
              this.props.history.goBack();
          }, 1000)
      } catch(e){
         toast(e.response.data);
      }
    };
    render() {
      return (
        <div className="container">
        <Formik
          validationSchema={schema}
          onSubmit={(values, { setSubmitting }) => {
            this.handleSubmit(values);
            setSubmitting(false);
          }}
          initialValues={{
            vecchiaPassword: "",
            password: "",
            confirmPassword: ""
          }}
        >
          {({
            handleSubmit,
            handleChange,
            handleBlur,
            values,
            touched,
            isValid,
            dirty,
            errors,
          }) => (
            <div className="Container" id="modifica-password">
              <div className="wrapper">
                <ToastContainer />
                <Card id="cardmodificapassword">
                  <Card.Header id="cardheadermodificapassword">
                    Modifica Password
                  </Card.Header>
                  <Card.Body>
                    <Form
                      noValidate
                      onSubmit={handleSubmit}
                      id="formmodificapassword"
                    >
                      <Form.Row>
                        <Form.Group controlId="formBasicPassword">
                          <Form.Control
                            className="SignUpFormControls"
                            size="lg"
                            type="password"
                            name="vecchiaPassword"
                            value={values.vecchiaPassword}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.vecchiaPassword && errors.vecchiaPassword
                                ? "error"
                                : null
                            }
                            placeholder="Vecchia Password"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.password}
                          </Form.Control.Feedback>
                        </Form.Group>
                    </Form.Row>
                      <Form.Row>
                        <Form.Group controlId="formBasicPassword">
                          <Form.Control
                            className="SignUpFormControls"
                            size="lg"
                            type="password"
                            name="password"
                            value={values.password}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.password && errors.password
                                ? "error"
                                : null
                            }
                            placeholder="Nuova Password"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.password}
                          </Form.Control.Feedback>
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group controlId="formBasicConfirmPassword">
                          <Form.Control
                            className="SignUpFormControls"
                            size="lg"
                            name="confirmPassword"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            isInvalid={
                              touched.confirmPassword &&
                              errors.confirmPassword
                                ? "error"
                                : null
                            }
                            value={values.confirmPassword}
                            placeholder="Conferma Password"
                            type="password"
                          />
                          <Form.Control.Feedback
                            className="FeedBack"
                            type="invalid"
                          >
                            {errors.confirmPassword}
                          </Form.Control.Feedback>
                        </Form.Group>
                      </Form.Row>
                      <Form.Row>
                        <Form.Group id="registrati">
                          <Button variant="primary" type="submit">
                            Modifica
                          </Button>
                        </Form.Group>
                      </Form.Row>
                    </Form>
                  </Card.Body>
                </Card>
              </div>
            </div>
          )}
        </Formik>
         </div>
      );

    }
  }

