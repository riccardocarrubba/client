import React from "react";
import {Card, Col, Row, Carousel} from "react-bootstrap";
import '../css/infoStruttura.css';
import {Config} from '../config/config'
import {FaToilet, FaDog} from 'react-icons/fa';
import {GiCigarette} from 'react-icons/gi';
import {MdEuroSymbol} from "react-icons/md";



export default class VisualizzaCamera extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            camera: props.location.state
        }
    }

    render() {
        return (
            <div className="container">
                {this.state.camera ? <div>
                        <Card  className="card-appearance" id="infostruttura">
                            {this.state.camera.path ?
                                <Carousel >
                                        <Carousel.Item>
                                            <img  id="struttura-img"
                                                className="d-block w-100 "
                                                src={Config.IMAGE_URL + this.state.camera.path}
                                            />
                                        </Carousel.Item>
                                </Carousel>
                                : null}


                            <Card.Body>
                                <Card.Title><h2>{this.state.camera.nome}</h2></Card.Title>
                                    <Row>
                                        <Col xs="12" sm="12" md="6" lg="4">
                                            <h4>Descrizione: </h4>
                                            <ul>{this.state.camera.descrizione}  </ul>
                                        </Col>
                                        <Col xs="12" sm="12" md="3" lg="4">
                                            <h4>I servizi della camera:</h4>
                                            <ul>{this.state.camera.area_fumatori ?
                                                <span><GiCigarette fontSize={25}/>Comprende un'area fumatori</span> : null} </ul>
                                            <ul>{this.state.camera.animali ?
                                                <span><FaDog fontSize={25}/> Gli animali possono entrare</span> : null}</ul>
                                            <ul>{this.state.camera.bagno_disabili ?
                                                <span><FaToilet fontSize={25}/>E' presente un bagno per disabili</span> : null}</ul>
                                        </Col>
                                        <Col xs="12" sm="12" md="3" lg="4">
                                            <h4>Prezzo della camera a notte:</h4>
                                            <ul className="prezzo"><MdEuroSymbol id="prezzo" fontSize={20} />{this.state.camera.prezzo}</ul>
                                        </Col>

                                    </Row>

                            </Card.Body>
                        </Card>
                    </div>
                    :
                    <div> nessuna camera selezionata </div>}
            </div>
        );
    }
};
