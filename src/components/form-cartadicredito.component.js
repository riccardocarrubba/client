import React from "react";
import { Formik } from "formik";
import "../css/GestionePrenotazioni.css";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";
import Cards from "react-credit-cards";
import "react-credit-cards/es/styles-compiled.css";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import * as yup from "yup";
import BookService from "../services/book.service";

const schema = yup.object().shape({
    cvc: yup.string().required("Per favore, inserisci il cvc").matches(/^[0-9]{3,3}$/,"Deve contenere 3 numeri."),
    numero_carta: yup.string().required("Inserisci il numero della carta").matches(/^[0-9]{16,16}$/, "Deve contenere 16 numeri."),
    nome_proprietario: yup.string().required("Inserisci il titolare della carta").matches(/^[A-Za-z]{0,16}$/, "Deve contenere solo caratteri."),
});


export default class FormCartadicreditoComponent extends React.Component {
    constructor(props) {
        super(props);
    }
    state ={focus:""}
    handleInputFocus = (e) => {
        this.setState({ focus: e.target.name });
    };

    render() {
        let anno_corrente = new Date().getFullYear()
        return (
            <div className="container">
                <ToastContainer />
                <Formik
                    validationSchema={schema}
                    onSubmit={async (values, { setSubmitting }) => {
                        try {
                            await BookService.checkCreditCard(values)
                            toast("pagamento avvenuto con successo");
                            setSubmitting(true);
                            setTimeout(() => {
                                this.props.history.push("/home");
                            },3000)
                        } catch (e) {
                        }
                    }}
                    initialValues={{
                        nome_proprietario: "",
                        numero_carta : "",
                        data_scadenza: "01" + (anno_corrente + 1).toString().substring(2),
                        mese_scadenza: "01",
                        anno_scadenza: anno_corrente.toString(),
                        cvc: "",
                    }} >
                    {({
                          handleSubmit,
                          handleChange,
                          handleBlur,
                          values,
                          setFieldValue,
                          touched,
                          isValid,
                          errors,
                      }) => (
                        <div className="page-container" id="registrazione">
                            <div id="PaymentForm">
                                <Cards
                                    cvc={values.cvc}
                                    expiry={values.data_scadenza}
                                    focused={this.state.focus}
                                    name={values.nome_proprietario}
                                    number={values.numero_carta}
                                />
                                <Form noValidate id="formpagamento" onSubmit={handleSubmit} >

                                    <Form.Group style={{marginTop: "50px"}}>
                                        <Form.Label as="legend">
                                            Numero Carta Di Credito:
                                        </Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Numero Carta Di Credito"
                                            lable="Numero Carta"
                                            aria-describedby="inputGroupPrepend"
                                            name="numero_carta"
                                            value={values.prezzo}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            onFocus={this.handleInputFocus}
                                            isInvalid={ touched.numero_carta && errors.numero_carta ? "error" : null}
                                        />
                                        <Form.Control.Feedback
                                            className="FeedBack"
                                            type="invalid" >
                                            {errors.numero_carta}
                                        </Form.Control.Feedback>

                                    </Form.Group>


                                    <Form.Group>
                                        <Form.Label as="legend">Nome Proprietario: </Form.Label>
                                        <Form.Group>
                                            <Form.Control
                                                type="text"
                                                placeholder="Nome Proprietario"
                                                value={values.nome_proprietario}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                onFocus={this.handleInputFocus}
                                                name="nome_proprietario"
                                                className="SignUpFormControls"
                                                size="lg"
                                                isInvalid={ touched.nome_proprietario && errors.nome_proprietario ? "error" : null}
                                            />
                                            <Form.Control.Feedback
                                                className="FeedBack"
                                                type="invalid" >
                                                {errors.nome_proprietario}
                                            </Form.Control.Feedback>
                                        </Form.Group>
                                        <Form.Label as="legend">CVC: </Form.Label>
                                        <Form.Group controlId="CVC">
                                            <Form.Control
                                                type="text"
                                                name="cvc"
                                                value={values.cvc}
                                                placeholder="Card Security Code: "
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                onFocus={this.handleInputFocus}
                                                isInvalid={ touched.cvc && errors.cvc ? "error" : null}
                                            />
                                            <Form.Control.Feedback
                                                className="FeedBack"
                                                type="invalid" >
                                                {errors.cvc}
                                            </Form.Control.Feedback>
                                        </Form.Group>
                                    </Form.Group>

                                    <Form.Group
                                        id="mese_scadenza"
                                        controlId="formGridState">
                                        <Form.Label as="legend">
                                            Mese di Scadenza
                                        </Form.Label>
                                        <Form.Control
                                            as="select"
                                            name="mese_scadenza"
                                            value={values.mese_scadenza}
                                            onChange={ (e) => {
                                                handleChange(e);
                                                if (e.target.value !== '' && values.anno_scadenza !== ''){
                                                    setFieldValue('data_scadenza', e.target.value + (values.anno_scadenza).toString().substring(2))
                                                }
                                            }}
                                            onFocus={this.handleInputFocus}
                                            onBlur={handleBlur} >
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group
                                        id="anno_scadenza"
                                        controlId="formGridState">
                                        <Form.Label as="legend">
                                            Anno di Scadenza
                                        </Form.Label>
                                        <Form.Control
                                            as="select"
                                            name="anno_scadenza"
                                            value={values.anno_scadenza}
                                            onChange={(e) => {
                                                handleChange(e);
                                                if (e.target.value !== '' && values.mese_scadenza !== '') {
                                                    setFieldValue('data_scadenza', values.mese_scadenza + (e.target.value).toString().substring(2))
                                                }
                                            }}
                                            onFocus={this.handleInputFocus}
                                            onBlur={handleBlur}>
                                            <option>{anno_corrente.toString() }</option>
                                            <option>{(anno_corrente + 1).toString() }</option>
                                            <option>{(anno_corrente + 2).toString() }</option>
                                            <option>{(anno_corrente + 3).toString() }</option>
                                            <option>{(anno_corrente + 4).toString() }</option>
                                            <option>{(anno_corrente + 5).toString() }</option>
                                        </Form.Control>
                                    </Form.Group>



                                    <Form.Row>
                                        <Form.Group>
                                            <Button
                                                id="richiesta-prenotazione-button"
                                                variant="primary"
                                                type="submit"
                                            >
                                                Paga
                                            </Button>
                                        </Form.Group>
                                    </Form.Row>
                                </Form>
                            </div>
                        </div>
                    )}
                </Formik>
            </div>
        );
    }
}
