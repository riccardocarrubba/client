
import React from "react";
import '../css/MostraListaStrutture.css';
import bookService from '../services/book.service'
import authService from "../services/auth.service";
import Table from 'react-bootstrap/Table'


export default class StoricoPrenotazioni extends React.Component {
    state = {
        listReservation: [],
        show: false,
        index: undefined
    }
    constructor(props) {
        super(props)
        this.loadData()
    }
    loadData = async () => {
        try {
            let user = authService.getCurrentUser()
            let response = await bookService.getHistoryBooksByUser(user.id)
            this.setState({listReservation: response.data})
            this.handleShow(false)
        } catch (e) {
        }
    }



    handleShow = (state) => {this.setState({show: state})}



    render() {
        return (
            <div className="container">
                            <div id="tabella-prenotazioni">
                                <Table responsive>
                                <thead>
                                    <tr>
                                    <th>ID BOOKING</th>
                                    <th>NOME</th>
                                    <th>COGNOME</th>
                                    <th>E-MAIL</th>
                                    <th>STRUTTURA</th>
                                    <th>CAMERA</th>
                                    <th>CHECK-IN</th>
                                    <th>CHECK-OUT</th>
                                    <th>TOTALE</th>
                                    </tr>
                                </thead>
                                {this.state.listReservation.map((prenotazione, index) =>
                                <tbody  key= {index}>
                                    <tr>
                                    <td>{prenotazione.id}</td>
                                    <td>{prenotazione.nome_utente}</td>
                                    <td>{prenotazione.cognome_utente}</td>
                                    <td>{prenotazione.mail_utente}</td>
                                    <td>{prenotazione.nome_struttura}</td>
                                    <td>{prenotazione.id_camera !== null ? prenotazione.nome_camera : " CASA VACANZA "}</td>
                                    <td>{prenotazione.check_in}</td>
                                    <td>{prenotazione.check_out}</td>
                                    <td>{prenotazione.prezzo}</td>
                                    <td>
                                    </td>
                                    </tr>


                                </tbody>
                                )}
                                </Table>
                            </div>
            </div>
        );
    }
};
