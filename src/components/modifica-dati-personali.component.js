import React from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import * as yup from "yup";
import {Formik} from "formik";
import Card from "react-bootstrap/Card";
import AuthService from "../services/auth.service";
import {ToastContainer, toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../css/AreaPersonale.css";

const validate_birthday = yup.string().required('La data deve essere immessa')
    .test({
        name: "DOB",
        message: "Devi aver compiuto almeno 18 anni!",
        test: value => {
            let now = Date.now()
            let date = Date.parse(value)
            return  (new Date(now - date).getUTCFullYear() - 1970) >= 18
        }
    });

const schema = yup.object({
    nome: yup.string().required("Per favore, inserisci il tuo nome"),
    cognome: yup.string().required("Per favore, inserisci il tuo cognome"),
    data_nascita: validate_birthday,
    citta: yup.string().required("Per favore, inserisci la tua città"),
    mail: yup.string().email().required("Per favore, inserisci la tua email"),
    telefono: yup.string().matches(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/, "Deve contenere 10 interi.")
});

export default class ModificaDatiPersonali extends React.Component {

    render() {
        return (
            <div className="container">
            <div id="modifica-dati-personali">
                <Formik
                    initialValues={AuthService.getCurrentUser()}
                    validationSchema={schema}
                    onSubmit={async (values) => {
                        try {
                            let token = AuthService.getCurrentUser().token
                            let response = await AuthService.modify(values)
                            response.data.token = token
                            localStorage.setItem("user", JSON.stringify(response.data))
                            toast("Il tuo profilo è stato correttamente aggiornato")
                        }
                        catch (e) {
                            toast(e.response.data);
                        }
                    }}>
                    {({
                          values,
                          errors,
                          touched,
                          handleChange,
                          handleBlur,
                          handleSubmit,
                          isSubmitting,
                      }) => (
                        <div className="Container" id="registrazione">
                            <ToastContainer/>
                            <Card id="registrazionecard">
                                <Card.Header id="cardheaderregistrazione">
                                    Modifica i tuoi dati
                                </Card.Header>
                                <Card.Body>
                                    <Form onSubmit={handleSubmit}>
                                        <Form.Row>
                                            <Form.Group controlId="nome">
                                                <Form.Control
                                                    name="nome"
                                                    type="text"
                                                    placeholder="Nome"
                                                    value={values.nome}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    isInvalid={touched.nome && errors.nome ? "error" : null}
                                                    size="lg"
                                                />
                                                <Form.Control.Feedback
                                                    className="FeedBack"
                                                    type="invalid">
                                                    {errors.nome}
                                                </Form.Control.Feedback>

                                            </Form.Group>
                                            <Form.Group controlId="cognome">
                                                <Form.Control
                                                    type="text"
                                                    placeholder="Cognome"
                                                    value={values.cognome}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    name="cognome"
                                                    isInvalid={touched.cognome && errors.cognome ? "error" : null}
                                                    size="lg"
                                                />
                                                <Form.Control.Feedback
                                                    className="FeedBack"
                                                    type="invalid">
                                                    {errors.cognome}
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                            <Form.Group controlId="citta">
                                                <Form.Control
                                                    type="text"
                                                    placeholder="Città"
                                                    value={values.citta}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    name="citta"
                                                    isInvalid={touched.citta && errors.citta ? "error" : null}
                                                    size="lg"
                                                />
                                                <Form.Control.Feedback
                                                    className="FeedBack"
                                                    type="invalid">
                                                    {errors.citta}
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                        </Form.Row>
                                        <Form.Row>
                                            <Form.Group controlId="data_nascita">
                                                <Form.Control
                                                    size="lg"
                                                    isInvalid={touched.data_nascita && errors.data_nascita ? "error" : null}
                                                    type="date"
                                                    name="data_nascita"
                                                    value={values.data_nascita}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    placeholder="Data di nascita"
                                                />
                                                <Form.Control.Feedback
                                                    className="FeedBack"
                                                    type="invalid">
                                                    {errors.data_nascita}
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                            <Form.Group controlId="telefono">
                                                <Form.Control
                                                    type="text"
                                                    placeholder="Telefono"
                                                    value={values.telefono}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    name="telefono"
                                                    isInvalid={touched.telefono && errors.telefono ? "error" : null}
                                                    size="lg"
                                                />
                                                <Form.Control.Feedback
                                                    className="FeedBack"
                                                    type="invalid">
                                                    {errors.telefono}
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                        </Form.Row>
                                        <Form.Row>
                                            <Form.Group controlId="mail">
                                                <Form.Control
                                                    type="email"
                                                    placeholder="Email"
                                                    value={values.mail}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    name="mail"
                                                    isInvalid={touched.mail && errors.mail ? "error" : null}
                                                    size="lg"
                                                    disabled
                                                />
                                                <Form.Control.Feedback
                                                    className="FeedBack"
                                                    type="invalid">
                                                    {errors.mail}
                                                </Form.Control.Feedback>
                                            </Form.Group>
                                        </Form.Row>
                                        <Button variant="primary" type="submit">
                                            Modifica
                                        </Button>
                                    </Form>
                                </Card.Body>
                            </Card>
                        </div>
                    )}
                </Formik>
            </div>
            </div>
        );
    }
}
