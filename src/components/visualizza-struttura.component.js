import React from "react";
import { Button, Card, Col, Row, Carousel } from "react-bootstrap";
import '../css/GestioneStruttura.css';
import cameraService from '../services/camere.service'
import { Config } from '../config/config'
import authService from '../services/auth.service';
import { AiFillPhone, AiOutlineWifi } from 'react-icons/ai';
import { GiLift } from 'react-icons/gi';
import { FaParking, FaToilet, FaDog } from 'react-icons/fa';
import { GiCigarette } from 'react-icons/gi';
import { MdEuroSymbol } from "react-icons/md";
import Modal from 'react-bootstrap/Modal';


export default class VisualizzaStruttura extends React.Component {


    constructor(props) {
        super(props)

        if (props.location.state !== undefined) {
            this.state = {
                structure: props.location.state.struttura,
                show: false,
                permanenza_giorni: props.location.state.permanenza_giorni,
                check_in: props.location.state.check_in,
                check_out: props.location.state.check_out,
                rooms: [],
                camera: null
            }
            if (this.state.structure) {
                this.loadData(this.state.structure.id_struttura)

            }
        }


    }

    loadData = async (id_struttura) => {
        try {
            let response = await cameraService.getRoomsByStructure(id_struttura)
            this.setState({ rooms: response.data })
        } catch (e) {
        }

    }

    handleShow = (state) => { this.setState({ show: state }) }

    render() {
        return (
            <div className="container">
                {this.state ?
                    <div>
                        <Card className="card-appearance" id="infostruttura">
                            {this.state.structure.s_paths.length > 0 ?
                                <Carousel >
                                    {this.state.structure.s_paths.map((image, index) =>
                                        <Carousel.Item key={`car-` + index}>
                                            <img id="struttura-img"
                                                className="d-block w-100 "
                                                src={Config.IMAGE_URL + image.path}
                                                alt={index + "slide"}
                                            />
                                        </Carousel.Item>
                                    )}
                                </Carousel>
                                : null}


                            <Card.Body>
                                <Card.Title><h3>{this.state.structure.s_nome}</h3></Card.Title>
                                 {this.state.structure.s_descrizione}
                                    <Row>{this.state.structure.wifi ?
                                        <span><AiOutlineWifi fontSize={25} /> E' presente un wi-fi</span> : <span> </span>}</Row>
                                    <Row>{this.state.structure.parcheggio ?
                                        <span><FaParking fontSize={25} /> E' presente un parcheggio</span> : <span> </span>}</Row>
                                    <Row>{this.state.structure.ascensore_disabili ?
                                        <span><GiLift fontSize={25} /> La struttura è dotata di ascensore</span> : <span> </span>}</Row>

                                    <span>Vuoi contattare il proprietario?</span>
                                    <span><AiFillPhone fontSize={25} />{this.state.structure.telefono}</span>
                                {
                                    this.state.structure.tipologia === 2 ?
                                        authService.getCurrentUser() ?
                                            authService.getCurrentUser().tipologia === true ?
                                                <div className="registrati-prenotazione">
                                                    <span className="registrati-prenotazione">Per prenotare devi effettuare l'accesso come cliente!</span>
                                                    <br />
                                                    <a className="registrati-prenotazione" onClick={
                                                        () =>{
                                                            authService.logout()
                                                            this.props.history.replace('login')
                                                            window.location.reload(false);
                                                        }
                                                    }>Log Out</a>
                                                </div>
                                                :

                                                <Row>
                                                    <Button className="prenota-button" id="modifica" variant="primary small-button" type="button"
                                                        onClick={() => {
                                                            const data = {
                                                                check_in: this.state.check_in,
                                                                check_out: this.state.check_out,
                                                                struttura: this.state.structure,
                                                                permanenza_giorni: this.state.permanenza_giorni,
                                                                prezzo: this.state.structure.prezzo
                                                            }
                                                            this.props.history.push({
                                                                pathname: '/richiestaPrenotazione',
                                                                state: {
                                                                    check_in: data.check_in,
                                                                    check_out: data.check_out,
                                                                    struttura: data.struttura,
                                                                    permanenza_giorni: data.permanenza_giorni,
                                                                    prezzo: data.prezzo
                                                                }
                                                            })
                                                        }}>Prenota
                                            </Button>
                                                </Row>
                                            :
                                            <div className="registrati-prenotazione">
                                                <span className="registrati-prenotazione">Per prenotare devi effettuare l'accesso come cliente!</span>
                                                <br />
                                                <a className="registrati-prenotazione" href="/login" onClick={
                                                    () =>{
                                                        authService.logout()
                                                        this.props.history.replace('login')
                                                        window.location.reload(false);
                                                    }
                                                }>accedi</a>
                                                <br />
                                                oppure
                                                <br />
                                                <a className="registrati-prenotazione" href="/register" onClick={
                                                    () =>{
                                                        authService.logout()
                                                        this.props.history.replace('login')
                                                        window.location.reload(false);
                                                    }
                                                }>registrati come cliente</a>
                                            </div>
                                        : null
                                }
                            </Card.Body>
                        </Card>


                        <Modal show={this.state.show} onHide={() => this.handleShow(false)}>
                            {this.state.camera !== null ? <> <Modal.Header closeButton>
                                            <Modal.Title>{this.state.camera.nome}</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            <Row>
                                                <p> {this.state.camera.descrizione} </p>
                                            </Row>
                                            <hr />
                                                    <h5>I servizi della camera:</h5>
                                                    <Row>
                                                         <ul>{this.state.camera.area_fumatori ?
                                                        <span><GiCigarette fontSize={25} />Comprende un'area fumatori</span> : null} </ul>
                                                    </Row>
                                                    <Row>
                                                        <ul>{this.state.camera.animali ?
                                                        <span><FaDog fontSize={25} /> Gli animali possono entrare</span> : null}</ul>
                                                    </Row>
                                                   <Row>
                                                        <ul>{this.state.camera.bagno_disabili ?
                                                        <span><FaToilet fontSize={25} />E' presente un bagno per disabili</span> : null}</ul>
                                                    </Row>
                                            <hr />
                                            <Row>
                                                <span>Prezzo della camera a notte:</span>
                                                    <ul className="prezzo"><MdEuroSymbol id="prezzo" fontSize={20} />{this.state.camera.prezzo}</ul>
                                            </Row>

                                        </Modal.Body>
                                        <Modal.Footer>
                                            <Button variant="primary small-button" onClick={() => { this.handleShow(false) }} >
                                                Chiudi
                                            </Button>
                                        </Modal.Footer></> : null}

                                    </Modal>


                        {this.state.rooms.map((room, index) =>
                            <Row className="lista-camere" key={'room-'+index}>
                                <Col className="immagine" xs="12" sm="12" md="4" lg="4">
                                    <img className="card-img" src={Config.IMAGE_URL + room.path} />
                                </Col>
                                <Col className="descrizione" xs="12" sm="12" md="5" lg="5">
                                    <Row>
                                        <h3><strong fontSize={20}>{room.nome}</strong></h3>
                                    </Row>

                                    <Button variant="secondary small-button" type="button"
                                        onClick={() => {
                                            this.setState({ camera: room })
                                            this.handleShow(true) }}
                                        onMouseDown={() => { this.setState({ index: index, camera_id: room.id }) }}>
                                        Informazioni sulla camera
                                                </Button>


                                    <Row>
                                        <h3>Prezzo della camera a notte:
                                                        <span className="prezzo">{room.prezzo}<MdEuroSymbol id="prezzo" fontSize={20} /></span>
                                        </h3>
                                    </Row>
                                </Col>
                                <Col xs="12" sm="12" md="3" lg="3">

                                    {authService.getCurrentUser() ?
                                        authService.getCurrentUser().tipologia === true ?
                                            <div className="registrati-prenotazione">
                                                <span className="registrati-prenotazione">Per prenotare devi effettuare l'accesso come cliente!</span>
                                                <br/>
                                                <a className="registrati-prenotazione" onClick={() => {
                                                    localStorage.clear()
                                                    this.props.history.push("/login");
                                                    window.location.reload(false);
                                                }}>Log Out</a>
                                            </div>
                                            :
                                            <Row>
                                                <Button className="prenota-button" id="modifica"
                                                        variant="primary small-button" type="button"
                                                        onClick={() => {
                                                            const data = {
                                                                check_in: this.state.check_in,
                                                                check_out: this.state.check_out,
                                                                struttura: this.state.structure,
                                                                permanenza_giorni: this.state.permanenza_giorni,
                                                                prezzo: room.prezzo
                                                            }
                                                            this.props.history.push({
                                                                pathname: '/richiestaPrenotazione',
                                                                state: {
                                                                    check_in: data.check_in,
                                                                    check_out: data.check_out,
                                                                    struttura: data.struttura,
                                                                    permanenza_giorni: data.permanenza_giorni,
                                                                    prezzo: data.prezzo
                                                                }
                                                            })
                                                        }}>Prenota
                                                </Button>
                                            </Row>
                                        :
                                        <div className="registrati-prenotazione">
                                            <span className="registrati-prenotazione">Per prenotare devi effettuare l'accesso!</span>
                                            <br/>
                                            <a className="registrati-prenotazione" href="/login">accedi</a>
                                            <br/>
                                            oppure
                                            <br/>
                                            <a className="registrati-prenotazione" href="/register">registrati</a>
                                        </div>
                                    }
                                </Col>
                            </Row>
                        )}
                    </div>
                    :
                    <div> nessuna struttura selezionata </div>}
            </div>
        );
    }
};
