import Button from "react-bootstrap/Button";
import React from "react";
import '../css/GestionePrenotazioni.css';
import bookService from '../services/book.service'
import authService from "../services/auth.service";
import {ToastContainer, toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Table from 'react-bootstrap/Table';
import {AiOutlineClose, AiOutlineCheck} from 'react-icons/ai';

export default class PrenotazionePending extends React.Component {
    state = {
        listReservation: [],
        show: false,
        index: undefined
    }
    constructor(props) {
        super(props)
        this.loadData()
    }
    loadData = async () => {
        try {
            let user = authService.getCurrentUser()
            let response = await bookService.getPendingBooksByUser(user.id)
            this.setState({listReservation: response.data})
            this.handleShow(false)
        } catch (e) {
        }
    }

    rifiutaPrenotazione = async (prenotazione, index) => {

        try{
            let _obj = {...prenotazione}
            delete _obj.attesa
            delete _obj.check_in
            delete _obj.check_out
            delete _obj.cognome_utente
            delete _obj.creata_il
            delete _obj.id_camera
            delete _obj.id_proprietario
            delete _obj.id_struttura
            delete _obj.nome_camera
            delete _obj.prezzo
            delete _obj.tassa
            delete _obj.tipologia_ospite
            delete _obj.id_cliente
            let response = await bookService.deleteBooksById(_obj)
            let prenotazioni = this.state.listReservation;
            await toast("La prenotazione " + prenotazioni[index].id + " è stata eliminata");
            prenotazioni.splice(index, 1);
            this.setState({listReservation: prenotazioni, show: false})
        } catch(e){
            toast(e.response.data);
        }


    }

    accettaPrenotazione = async (prenotazione, index) => {

        try{
            let _obj = {...prenotazione}
            delete _obj.attesa
            delete _obj.cognome_utente
            delete _obj.creata_il
            delete _obj.id_proprietario
            delete _obj.nome_camera
            delete _obj.prezzo
            delete _obj.tassa
            delete _obj.tipologia_ospite
            delete _obj.id_cliente
            let response = await bookService.acceptBooks(_obj)
            let prenotazioni = this.state.listReservation;
            await toast("La prenotazione " + prenotazioni[index].id + " è stata accettata");
            prenotazioni.splice(index, 1);
            this.setState({listReservation: prenotazioni, show: false})
        } catch(e){
            toast(e.response.data);
        }


    }




    handleShow = (state) => {this.setState({show: state})}



    render() {
        return (
            <div className="container">

                <ToastContainer/>

                        <div id="tabella-prenotazioni">
                                    <Table responsive>
                                            <thead>
                                                <tr>
                                                <th>ID BOOKING</th>
                                                <th>NOME</th>
                                                <th>COGNOME</th>
                                                <th>E-MAIL</th>
                                                <th>STRUTTURA</th>
                                                <th>CAMERA</th>
                                                <th>CHECK-IN</th>
                                                <th>CHECK-OUT</th>
                                                <th>TOTALE</th>
                                                <th>RISPONDI:</th>
                                                </tr>
                                            </thead>
                                            {this.state.listReservation.map((prenotazione, index) =>
                                            <tbody  key= {index}>
                                                <tr>
                                                <td>{prenotazione.id}</td>
                                                <td>{prenotazione.nome_utente}</td>
                                                <td>{prenotazione.cognome_utente}</td>
                                                <td>{prenotazione.mail_utente}</td>
                                                <td>{prenotazione.nome_struttura}</td>
                                                <td>{prenotazione.id_camera !== null ? prenotazione.nome_camera : " CASA VACANZA "}</td>
                                                <td>{prenotazione.check_in}</td>
                                                <td>{prenotazione.check_out}</td>
                                                <td>{prenotazione.prezzo}</td>
                                                <td>
                                                <Button className="prenotazioni-pending" variant="success" type="button"
                                                    onClick={() => {this.accettaPrenotazione(prenotazione, index)} }><AiOutlineCheck />
                                                </Button>
                                                <Button className="prenotazioni-pending" variant="danger" type="button"
                                                    onClick={() => {this.rifiutaPrenotazione(prenotazione, index)}}><AiOutlineClose />
                                                </Button>
                                                </td>
                                                </tr>


                                            </tbody>
                                            )}
                                            </Table>

                                 </div>




            </div>
        );
    }
};
