import React from 'react';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import {FaEdit, FaExpeditedssl} from 'react-icons/fa';
import '../css/AreaPersonale.css';


export default class DatiPersonali extends React.Component {
    render() {
        return(
            <div className="container">
                <Form.Row id="carddatipersonali">
                    <Card style={{ width: '22rem' }} id="datipersonali">
                          <Card.Body id="carddatipersonali">
                              <Card.Title>
                              <FaEdit fontSize={30}/>
                              </Card.Title>
                              <Card.Subtitle className="mb-2 text-muted"><Card.Link id="cardutentelink" href="/modificaDatiPersonali"> Modifica Dati </Card.Link></Card.Subtitle>
                              <Card.Text>
                              Visualizza o modifica i tuoi dati personali.
                              </Card.Text>
                          </Card.Body>
                      </Card>
                        <Card style={{ width: '22rem' }} id="modificapassword">
                            <Card.Body id="cardmodificapassword">
                                <Card.Title>
                                    <FaExpeditedssl fontSize={30} />
                                </Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">
                                    <Card.Link id="cardutentelink" href="/modificaPassword" >Modifica Password </Card.Link>
                                </Card.Subtitle>
                                <Card.Text>
                              Modifica la tua password.
                              </Card.Text>
                            </Card.Body>
                        </Card>
                </Form.Row>

            </div>
        )
    }
}
