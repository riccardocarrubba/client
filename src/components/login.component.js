import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "../css/style.css";
import "../css/Autenticazione.css";
import Card from "react-bootstrap/Card";
import AuthService from "../services/auth.service";

export default class Accesso extends React.Component {
  state = {
    mail: "",
    password: "",
    error: "",
  };

  handleChangeEmail = (event) => {
    this.setState({ mail: event.target.value });
  };
  handleChangePassword = (event) => {
    this.setState({ password: event.target.value });
  };
  changeError = (value) => {
    this.setState({ error: value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    try {
      let user = this.state;
      delete user.error;
      let response = await AuthService.login(this.state);
      localStorage.setItem("user", JSON.stringify(response.data));
      this.props.history.push("/home");
      window.location.reload();

    } catch (e) {
      if (e.response !== null){
        if(e.response.data) {
          this.changeError(e.response.data)
        }
      }

    }
  };

  render() {
    return (
      <div className="Container">
        <div className="wrapper">
          <Card id="accessocard">
            <Card.Header id="cardheaderaccesso">Effettua l'accesso</Card.Header>
            <Card.Body>
              <Form onSubmit={this.handleSubmit}>
                <div className="row">
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Indirizzo E-mail</Form.Label>
                    <Form.Control
                      required
                      type="mail"
                      placeholder="Inserisci email"
                      onChange={this.handleChangeEmail}
                    />
                  </Form.Group>
                </div>
                <div className="row">
                  <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      required
                      type="password"
                      placeholder="Inserisci password"
                      onChange={this.handleChangePassword}
                    />
                  </Form.Group>
                </div>
                <div className="row">
                  <p id="errorep" style={{ color: "red" }}>
                    {this.state.error}
                  </p>
                </div>
                <div className="row">
                  <Button variant="primary" type="submit">
                    Accedi
                  </Button>
                </div>
              </Form>
            </Card.Body>
          </Card>
        </div>
      </div>
    );
  }
}
