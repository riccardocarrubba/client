import axios from "axios";
import { Config } from '../config/config'
import authService from '../services/auth.service';

class CustomerService {

    sendThreeMonthReport = (data) => {
        return axios.post(Config.API_URL + 'customers/sendThreeMonthReport', data,authService.getConfig() )
    }

    sendDataToPolice = (data) => {
        return axios.post(Config.API_URL + 'customers/sendDataToPolice', data,authService.getConfig())
    }
}

export default new CustomerService();
