import axios from "axios";
import {Config} from "../config/config";
import authService from '../services/auth.service';

class CamereService {

    createRoom(data) {
        let config = authService.getConfig()
        return axios.post(Config.API_URL + "rooms/createRoom", data, config);
    }

    getRoomsByStructure(id) {return axios.post(Config.API_URL + "rooms/getRoomsByStructure", {id});}

    modifyRooms = (obj) => {
        let config = authService.getConfig()
      return axios.put(Config.API_URL + "rooms/updateRooms", obj, config);
    };

      updateImages = (data) => {
      return axios.put(Config.API_URL + "rooms/updateImages", data);
    };


  deleteRooms = (room) => {
      let config = authService.getConfig()
    return axios.post(Config.API_URL + "rooms/deleteRoomsById", {
      room,
    }, config);
  }



}



export default new CamereService();
