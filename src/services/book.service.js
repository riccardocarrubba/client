import axios from "axios";
import {Config} from "../config/config";
import authService from '../services/auth.service';

class BookService {

  getHistoryBooksByUser = (id) => {
    let config = authService.getConfig()
    return axios.post(Config.API_URL + "books/getHistoryBooksByUser", {
      id,
    },config );
  };

  createBook = (data) => {
    let _data = { ...data };
    let config = authService.getConfig()
    delete _data.nome;
    delete _data.cognome;
    delete _data.email;
    delete _data.datadinascita;
    delete _data.descrizione;
    delete _data.numeroospiti;
    return axios.post(Config.API_URL + "books/createBook", {
      _data,
    },config);
  };

  getPendingBooksByUser = (id) => {
    let config = authService.getConfig()

    return axios.post(Config.API_URL + "books/getPendingBooksByUser", {
      id,
    }, config);
  };

  deleteBooksById = (prenotazione) => {
    let config = authService.getConfig()
    return axios.post(Config.API_URL + "books/deletePendingBooksById", {
      prenotazione,
    }, config);
  };

  acceptBooks = (prenotazione) => {
    let config = authService.getConfig()
    return axios.post(Config.API_URL + "books/acceptPendingBooks", {
      prenotazione,
    }, config);
  };

  checkCreditCard  = (dati_carta) => {
    return axios.post(Config.API_URL + "books/checkCreditCard", {dati_carta} );
  };

  allBooksCliente = (prenotazione) => {
    let config = authService.getConfig()
    return axios.post(Config.API_URL + "books/allBooksCliente", {
      prenotazione,
    }, config);
  };
  verifyDate = (obj) => {
    let config = authService.getConfig()
    return axios.post(Config.API_URL + "books/verifyDate", {
      obj,
    }, config);
  };
  verificaGuadagni = (id) => {
    let config = authService.getConfig()
    return axios.post(Config.API_URL + "books/verificaGuadagni", {id}, config);
  };
}


export default new BookService();
