  import axios from "axios";
  import {Config} from "../config/config";
  import authService from '../services/auth.service';

  class StruttureService {
    registraStruttura = (obj) => {
      let config = authService.getConfig()
      return axios.post(Config.API_URL + "structures/createStructure", obj,config);
    };
    aggiungiImmagine = (data) => {
      let config = authService.getConfig()
      return axios.post(Config.API_URL + "structures/uploadImage", data,config);
    };
    getAllStructures = (id) => {
      let config = authService.getConfig()
      return axios.post(Config.API_URL + "structures/getStructuresByUser", {
        id,
      },config);
    };
    deleteStructure = (id) => {
      let config = authService.getConfig()
      return axios.post(Config.API_URL + "structures/deleteStructureById", {
        id,
      },config);
    };
    ricerca(obj) {
      let _obj = { ...obj };
      delete _obj.focused;
      delete _obj.strutture;
      delete _obj.focusedInput;
      _obj.check_in = _obj.check_in.format("YYYY-MM-DD");
      _obj.check_out = _obj.check_out.format("YYYY-MM-DD");
      return axios.post(Config.API_URL + "structures/search", _obj);
    }

    modifyStructures = (obj) => {
      return axios.put(Config.API_URL + "structures/updateStructures", obj,authService.getConfig());
    };

      deleteImages = (obj) => {
      return axios.post(Config.API_URL + "structures/deleteImages", {obj},authService.getConfig());
    };
  }

export default new StruttureService();
