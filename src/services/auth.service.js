import axios from "axios";
import { Config } from '../config/config'

class AuthService {

  login = (obj) => {
    return axios.post(Config.API_URL + "auth/signin", obj);
  };

  logout() {
    localStorage.clear();
  }

  register = (obj) => {
    let _obj = { ...obj };
    delete _obj.confirmPassword;
    delete _obj.confirmEmail;
    return axios.post(Config.API_URL + "auth/signup", _obj);
  };

  modify = (obj) => {
    let _obj = { ...obj };
    let config = this.getConfig()
    return axios.put(Config.API_URL + "users/update", _obj, config );
  };

  getConfig = () => {
    let user = this.getCurrentUser()
    if (user !== null){
      let config = {
        headers: {
          Authorization: "Bearer " + user.token,
        },
      }
      return config;
    }
    else return {}
  }

  getCurrentUser = () => {
    return JSON.parse(localStorage.getItem("user"));
  };


  passModify = (obj) => {
    let _obj = { ...obj };
    delete _obj.confirmPassword;
    let config = this.getConfig()
    return axios.put(Config.API_URL + "users/passModify", _obj, config);
  }
}

export default new AuthService();
