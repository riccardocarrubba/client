import React, {Component} from "react";
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import "./css/style.css";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Spinner from 'react-bootstrap/Spinner';
import FormAggiungiCamera from "./components/aggiungicamera.component";
import AuthService from "./services/auth.service";
import Register from "./components/register.component";
import Home from "./components/home.component";
import FormAggiungiStruttura from "./components/aggiungistruttura.component";
import BoardCliente from "./components/board-cliente.component";
import BoardProprietario from "./components/board-proprietario.component";
import Login from "./components/login.component";
import GestioneStruttura from "./components/gestione-struttura.component";
import GestionePrenotazione from "./components/gestione-prenotazione.component";
import FormDatiQuesturaCliente from './components/form-dati-legali.component';
import ModificaDatiPersonali from "./components/modifica-dati-personali.component";
import StoricoPrenotazioni from "./components/storico-prenotazioni.component";
import PrenotazioniPending from "./components/prenotazioni-pending.component";
import FooterPage from "./components/footer.component";
import DatiPersonali from "./components/dati-personali.component";
import FomrRendicondoTrimestrale from "./components/rendiconto-trimestrale.component";
import DatiLegali from "./components/comunicazione-dati-legali.component";
import ModificaStruttura from "./components/modifica-strutture.component";
import ModificaCamera from "./components/modifica-camere.component";
import InfoStrutturaComponent from "./components/infoStruttura.component";
import ModificaPassword from "./components/modifica-password.component";
import { SiSnapcraft } from "react-icons/si";
import axios from "axios";
import { Config } from './config/config'
import VisualizzaStruttura from "./components/visualizza-struttura.component";
import VisualizzaCamera from "./components/visualizza-camera.component";
import RichiestaPrenotazione from "./components/richiesta-prenotazione.component";
import PrenotazioniEffettuate from "./components/prenotazioni-effettuate.component";
import FormCartadicreditoComponent from "./components/form-cartadicredito.component";


class App extends Component {
    state = {
        showClienteBoard: "",
        showProprietarioBoard: "",
        currentUser: "",
    };

    getOption = () => {
        const options = {
            headers: {
                Authorization: "Bearer " +AuthService.getCurrentUser(),
            },
        };
    }

    constructor(props) {
        super(props);
        this.logOut = this.logOut.bind(this);
        console.log = console.warn = console.error = () => {};
        this.state = {
            showClienteBoard: false,
            showProprietarioBoard: false,
            currentUser: undefined,
        };

        axios.interceptors.request.use(function (config) {
            document.getElementById('load_spinner').style.display = 'block';
            return config;
        }, function (error) {
            document.getElementById('load_spinner').style.display = 'none';
            return Promise.reject(error);
        });

        axios.interceptors.response.use(function (response) {
            document.getElementById('load_spinner').style.display = 'none';
            return response;
        }, function (error) {
            document.getElementById('load_spinner').style.display = 'none';
            if (localStorage.getItem('user') && error.config && error.response && error.response.status === 401) {
                let user = AuthService.getCurrentUser();
                return axios.post(Config.API_URL + 'auth/refreshToken', {user},
                    ).then((response) => {
                    error.config._retry = true;
                    localStorage.setItem('user', JSON.stringify(response.data))
                    error.config.headers['Authorization'] = "Bearer " + response.data.token;
                    return axios.request(error.config);
                }).catch((err) => {
                        return Promise.reject(error);
                    })
            }
            else {
                return Promise.reject(error);
            }


        });

    }



    componentDidMount() {
        const user = AuthService.getCurrentUser();
        if (user) {
            this.setState({
                currentUser: user,
                showClienteBoard: user.tipologia !== true,
                showProprietarioBoard: user.tipologia === true,
            });
        }

    }

    logOut() {
        AuthService.logout();
    }

    render() {
        const {currentUser, showClienteBoard, showProprietarioBoard} = this.state;
        return (
            <div className="page-container">

                <div className="content-wrap">
                    <Router>
                        <Navbar bg="dark" variant="dark" expand="lg" fixed="top">
                            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="mr-auto">
                                    <Nav.Item style={{color: "white"}}>
                                        <SiSnapcraft fontSize={24}/> <strong>Indigo</strong>
                                    </Nav.Item>
                                    <Nav.Item>
                                         <Nav.Link href="/home"> Home </Nav.Link>
                                    </Nav.Item>
                                    {showClienteBoard !== false && (
                                        <Nav.Item>
                                            <Nav.Link href="/cliente">Area Personale</Nav.Link>
                                        </Nav.Item>
                                    )}

                                    {showProprietarioBoard !== false && (
                                        <Nav.Item>
                                            <Nav.Link href="/proprietario">Area Personale</Nav.Link>
                                        </Nav.Item>
                                    )}


                                    {showProprietarioBoard !== false && (
                                        <Nav.Item>
                                            <Nav.Link href="/datiLegali">Comunicazione Dati Legali </Nav.Link>
                                        </Nav.Item>
                                    )}
                                    {currentUser === undefined && (
                                        <Nav.Item>
                                            <Nav.Link href="/register">Registrati </Nav.Link>
                                        </Nav.Item>
                                    )}
                                    {currentUser !== undefined ? (
                                        <Nav.Item>
                                            <Nav.Link href="/login" onClick={this.logOut}>LogOut</Nav.Link>
                                        </Nav.Item>
                                    ) : (
                                        <Nav.Item>
                                            <Nav.Link href="/login">Accedi</Nav.Link>
                                        </Nav.Item>
                                    )}
                                </Nav>
                            </Navbar.Collapse>
                        </Navbar>

                        <div style={{display:"flex"}}>
                            <Spinner id={'load_spinner'} animation="border" role="status">
                                <span className="sr-only">Loading...</span>
                            </Spinner>
                        </div>


                        <div className="container mt-3">

                            <Switch>
                                <Route exact path="/register" component={Register}/>
                                {showProprietarioBoard && (<Route exact path="/aggiungiStruttura" component={FormAggiungiStruttura}/>)}
                                {showProprietarioBoard && (<Route exact path="/aggiungiCamera" component={FormAggiungiCamera}/>)}
                                {showProprietarioBoard && (<Route exact path="/gestioneStruttura" component={GestioneStruttura}/>)}
                                {showProprietarioBoard && (<Route exact path="/gestionePrenotazione" component={GestionePrenotazione}/>)}
                                {showProprietarioBoard && <Route exact path="/datiQuesturaCliente" component={FormDatiQuesturaCliente}/>}
                                {showClienteBoard && <Route path="/cliente" component={BoardCliente}/>}
                                {showClienteBoard && <Route exact path="/prenotazioniEffettuate" component={PrenotazioniEffettuate}/>}
                                {showProprietarioBoard && <Route path="/proprietario" component={BoardProprietario}/>}
                                {showProprietarioBoard && <Route path="/modificaStruttura" component={ModificaStruttura}/>}
                                {showProprietarioBoard && <Route path="/modificaCamera" component={ModificaCamera}/>}
                                {showProprietarioBoard && <Route path="/visualizzaCamera" component={VisualizzaCamera}/>}
                                {showProprietarioBoard && <Route path="/datiLegali" component={DatiLegali}/>}
                                <Route exact path="/login" component={Login}/>
                                {showProprietarioBoard && <Route exact path="/infoStruttura" component={InfoStrutturaComponent}/>}
                                <Route exact path="/" component={Home}/>
                                <Route exact path="/home" component={Home}/>
                                {showProprietarioBoard && <Route exact path="/datiPersonali" component={DatiPersonali}/>}
                                {showClienteBoard && <Route exact path="/datiPersonali" component={DatiPersonali}/>}
                                {(showProprietarioBoard || showClienteBoard) && <Route exact path="/modificaDatiPersonali" component={ModificaDatiPersonali}/>}
                                {showProprietarioBoard && <Route exact path="/rendicontoTrimestrale" component={FomrRendicondoTrimestrale}/>}
                                {(showProprietarioBoard || showClienteBoard) && <Route exact path="/modificaPassword" component={ModificaPassword}/>}
                                <Route exact path="/visualizzaStruttura" component={VisualizzaStruttura}/>
                                {showClienteBoard && <Route exact path="/richiestaPrenotazione" component={RichiestaPrenotazione}/>}
                                {showClienteBoard && <Route exact path="/cartaDiCredito" component={FormCartadicreditoComponent}/>}
                                {showProprietarioBoard && <Route exact path="/storicoPrenotazioni" component={StoricoPrenotazioni}/>}
                                {showProprietarioBoard && <Route exact path="/prenotazioniPending" component={PrenotazioniPending}/>}
                            </Switch>

                        </div>


                    </Router>

                </div>
                <FooterPage/>
            </div>
        );
    }
}

export default App;

